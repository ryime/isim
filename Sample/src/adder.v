`timescale 1ns/1ps

module adder #(
        parameter WIDTH = 8
)(
        a,
        b,
        s
);
        input[WIDTH-1:0] a;
        wire[WIDTH-1:0] a;

        input[WIDTH-1:0] b;
        wire[WIDTH-1:0] b;

        output[WIDTH-1:0] s;
        wire[WIDTH-1:0] s;

        assign s = a + b;
endmodule
