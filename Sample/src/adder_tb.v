`timescale 1ns/1ps

module adder_tb;

reg[3:0] a,b;
wire[3:0] s;
adder #(4) myadder (a, b, s);

initial
begin
        a = 4'b0000;
        b = 4'b0000;
        
        #2;

        a = 4'b1010;
        b = 4'b0101;

        #2;

        a = 4'b0011;
        b = 4'b0101;

        #2;

        a = 4'b1111;
        b = 4'b0000;

        #2;
        $finish;
end

initial
begin
        $dumpfile("adder.vcd");
        $dumpvars;
end
endmodule
