QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    actionteam.cpp \
    codeeditor.cpp \
    codetextblockuserdata.cpp \
    comboboxitemdelegate.cpp \
    commandlineedit.cpp \
    compileprocess.cpp \
    consolebrowsertab.cpp \
    consoleerrortab.cpp \
    consoletabwidget.cpp \
    editortabwidget.cpp \
    hierarchyitem.cpp \
    hierarchymodel.cpp \
    main.cpp \
    mainwindow.cpp \
    menubar.cpp \
    newprojectwizard.cpp \
    newsourcewizard.cpp \
    nomarginlayout.cpp \
    preferencewidget.cpp \
    programconfig.cpp \
    project.cpp \
    projectfiletab.cpp \
    projectitem.cpp \
    projectpropertywidget.cpp \
    projectstructuretab.cpp \
    projecttabwidget.cpp \
    showcolorlineedit.cpp \
    showdirlineedit.cpp \
    showdirlistview.cpp \
    signalconnection.cpp \
    source.cpp \
    statusbar.cpp \
    toolbar.cpp \
    verilogcompleter.cpp \
    verilogsyntaxhighlighter.cpp \
    waveprocess.cpp

HEADERS += \
    actionteam.h \
    codeeditor.h \
    codetextblockuserdata.h \
    comboboxitemdelegate.h \
    commandlineedit.h \
    compileprocess.h \
    consolebrowsertab.h \
    consoleerrortab.h \
    consoletabwidget.h \
    editortabwidget.h \
    hierarchyitem.h \
    hierarchymodel.h \
    mainwindow.h \
    menubar.h \
    newprojectwizard.h \
    newsourcewizard.h \
    nomarginlayout.h \
    preferencewidget.h \
    programconfig.h \
    project.h \
    projectfiletab.h \
    projectitem.h \
    projectpropertywidget.h \
    projectstructuretab.h \
    projecttabwidget.h \
    showcolorlineedit.h \
    showdirlineedit.h \
    showdirlistview.h \
    signalconnection.h \
    source.h \
    statusbar.h \
    toolbar.h \
    verilogcompleter.h \
    verilogsyntaxhighlighter.h \
    waveprocess.h

FORMS +=

TRANSLATIONS += \
    EDA_zh_CN.ts
    EDA_en_CN.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icon.qrc \
    icon.qrc

DISTFILES +=
