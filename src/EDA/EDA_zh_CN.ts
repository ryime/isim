<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ConsoleBrowserTab</name>
    <message>
        <location filename="consolebrowsertab.cpp" line="18"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="consolebrowsertab.cpp" line="98"/>
        <source>Select a File</source>
        <translatorcomment>选择一个文件</translatorcomment>
        <translation>选择一个文件</translation>
    </message>
</context>
<context>
    <name>ConsoleTabWidget</name>
    <message>
        <location filename="consoletabwidget.cpp" line="18"/>
        <source>Compile</source>
        <translation>编译</translation>
    </message>
    <message>
        <location filename="consoletabwidget.cpp" line="19"/>
        <source>Wave</source>
        <translation>波形</translation>
    </message>
    <message>
        <location filename="consoletabwidget.cpp" line="20"/>
        <source>Error</source>
        <translation>错误列表</translation>
    </message>
</context>
<context>
    <name>EditorTabWidget</name>
    <message>
        <location filename="editortabwidget.cpp" line="62"/>
        <location filename="editortabwidget.cpp" line="168"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="62"/>
        <source>Cannot open file &quot;%1&quot; for reading.</source>
        <translation>无法打开文件以进行读入：&quot;%1&quot;。</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="114"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="115"/>
        <source>File: %1 has been modified. Save changes?</source>
        <translation>文件：%1有未保存的修改，是否保存？</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="158"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>无法写入文件：&quot;%1&quot;.
错误：%2。</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="162"/>
        <source>Cannot open file %1 for writing:
%2.</source>
        <translation>无法打开文件以进行写入：&quot;%1&quot;.
错误：%2。</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="176"/>
        <source>untitled_%1</source>
        <translation>未命名_%1</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="182"/>
        <source>Open Files</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="214"/>
        <location filename="editortabwidget.cpp" line="333"/>
        <source>Save File</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="322"/>
        <source>New File</source>
        <translation>新建文件</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="326"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="330"/>
        <source>Close File</source>
        <translation>关闭文件</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="337"/>
        <source>Save As File</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="340"/>
        <source>Save All File</source>
        <translation>保存全部</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="344"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="348"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="352"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="356"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="360"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="364"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="368"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="372"/>
        <source>Find &amp;&amp; Replace</source>
        <translation>查找与替换</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="376"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="380"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
</context>
<context>
    <name>FindWidget</name>
    <message>
        <location filename="editortabwidget.cpp" line="465"/>
        <source>Find:</source>
        <translation>查找：</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="467"/>
        <source>Find Previous</source>
        <translation>上一个</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="468"/>
        <source>Find Next</source>
        <translation>下一个</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="469"/>
        <source>Case sensitive</source>
        <translation>区分大小写</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="470"/>
        <source>Use Regular Expressions</source>
        <translation>正则表达式</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="471"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="482"/>
        <source>Replace With:</source>
        <translation>替换为：</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="484"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="485"/>
        <source>Replace and Find</source>
        <translation>替换并查找</translation>
    </message>
    <message>
        <location filename="editortabwidget.cpp" line="486"/>
        <source>Replace All</source>
        <translation>替换全部</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="obsolete">主窗口</translation>
    </message>
    <message>
        <source>Close Project</source>
        <translation type="obsolete">关闭项目</translation>
    </message>
    <message>
        <source>Close File</source>
        <translation type="obsolete">关闭文件</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="obsolete">剪切</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="obsolete">复制</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="18"/>
        <source>iSim</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="26"/>
        <source>Editor</source>
        <translation>编辑器</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="35"/>
        <source>Console</source>
        <translation>控制台</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="21"/>
        <source>Project</source>
        <translation>工程</translation>
    </message>
    <message>
        <source>Add Source</source>
        <translation type="obsolete">添加源文件</translation>
    </message>
    <message>
        <source>Add Copy of Source</source>
        <translation type="obsolete">添加源文件的拷贝</translation>
    </message>
</context>
<context>
    <name>MenuBar</name>
    <message>
        <location filename="menubar.cpp" line="20"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="menubar.cpp" line="33"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="menubar.cpp" line="44"/>
        <source>View</source>
        <translation>视图</translation>
    </message>
    <message>
        <location filename="menubar.cpp" line="45"/>
        <source>Panel</source>
        <translation>窗口</translation>
    </message>
    <message>
        <location filename="menubar.cpp" line="49"/>
        <source>Zoom</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="menubar.cpp" line="54"/>
        <source>Project</source>
        <translation>工程</translation>
    </message>
    <message>
        <location filename="menubar.cpp" line="60"/>
        <source>Source</source>
        <translation>源文件</translation>
    </message>
    <message>
        <location filename="menubar.cpp" line="65"/>
        <source>Process</source>
        <translation>进程</translation>
    </message>
    <message>
        <location filename="menubar.cpp" line="74"/>
        <source>Tools</source>
        <translation>工具</translation>
    </message>
    <message>
        <location filename="menubar.cpp" line="75"/>
        <source>Preference</source>
        <translation>首选项</translation>
    </message>
</context>
<context>
    <name>NewProjectPage1</name>
    <message>
        <location filename="newprojectwizard.cpp" line="56"/>
        <source>Project Name:</source>
        <translation>工程名：</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="58"/>
        <source>Project Path:</source>
        <translation>工程路径：</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="58"/>
        <source>Browse</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="59"/>
        <source>Project Comment:</source>
        <translation>工程注释：</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="88"/>
        <source>Add Sources</source>
        <translation>

添加源文件</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="69"/>
        <source>Specify project name and path</source>
        <translation>设置工程名字及路径</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="102"/>
        <location filename="newprojectwizard.cpp" line="109"/>
        <location filename="newprojectwizard.cpp" line="114"/>
        <location filename="newprojectwizard.cpp" line="119"/>
        <source>New Project Wizard</source>
        <translation>新建工程向导</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="102"/>
        <source>Project &quot;%1&quot; is already existed, replace it?</source>
        <translation>工程：&quot;%1&quot;已存在，是否替换它？</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="109"/>
        <location filename="newprojectwizard.cpp" line="114"/>
        <location filename="newprojectwizard.cpp" line="119"/>
        <source>Cannot make path: %1</source>
        <translation>无法创建文件夹：%1</translation>
    </message>
</context>
<context>
    <name>NewProjectPage2</name>
    <message>
        <location filename="newprojectwizard.cpp" line="151"/>
        <source>Source List:</source>
        <translation>源文件列表：</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="vanished">浏览</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="151"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="151"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="154"/>
        <location filename="newprojectwizard.cpp" line="158"/>
        <source>Add Sources</source>
        <translation>添加源文件</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="177"/>
        <source>New Project Wizard</source>
        <translation>新建工程向导</translation>
    </message>
    <message>
        <location filename="newprojectwizard.cpp" line="177"/>
        <source>Cannot copy
%1
to
%2.</source>
        <translation>无法将
%1
复制到
%2.</translation>
    </message>
</context>
<context>
    <name>NewProjectWizard</name>
    <message>
        <location filename="newprojectwizard.cpp" line="21"/>
        <source>New Project Wizard</source>
        <translation>新建工程向导</translation>
    </message>
</context>
<context>
    <name>NewSourcePage1</name>
    <message>
        <location filename="newsourcewizard.cpp" line="77"/>
        <source>Source Name:</source>
        <translation>源文件名：</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="90"/>
        <source>Module Name:</source>
        <translation>模块名</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="92"/>
        <source>Source Path:</source>
        <translation>源文件路径：</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="92"/>
        <source>Browse</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="101"/>
        <source>Specify source name and path</source>
        <translation>设置源文件名及路径</translation>
    </message>
</context>
<context>
    <name>NewSourcePage2</name>
    <message>
        <location filename="newsourcewizard.cpp" line="155"/>
        <source>Set Timescale</source>
        <translation>设置时间刻度</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="179"/>
        <source>Set Parameters</source>
        <translation>设置参数</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="185"/>
        <location filename="newsourcewizard.cpp" line="217"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="186"/>
        <source>Default Value</source>
        <translation>默认值</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="188"/>
        <location filename="newsourcewizard.cpp" line="224"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="189"/>
        <location filename="newsourcewizard.cpp" line="225"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="208"/>
        <source>Set Ports</source>
        <translation>设置端口</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="214"/>
        <source>Direction</source>
        <translation>端口方向</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="215"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="216"/>
        <source>Sign</source>
        <translation>符号</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="218"/>
        <source>MSB</source>
        <translation>最高有效位（MSB）</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="219"/>
        <source>LSB</source>
        <translation>最低有效位（LSB）</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="257"/>
        <location filename="newsourcewizard.cpp" line="265"/>
        <location filename="newsourcewizard.cpp" line="271"/>
        <location filename="newsourcewizard.cpp" line="275"/>
        <location filename="newsourcewizard.cpp" line="286"/>
        <location filename="newsourcewizard.cpp" line="292"/>
        <location filename="newsourcewizard.cpp" line="296"/>
        <location filename="newsourcewizard.cpp" line="302"/>
        <source>New Source Wizard</source>
        <translation>新建源文件向导</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="257"/>
        <source>Time unit should not be less than time precision!</source>
        <translation>时间单位不能小于时间精度！</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="265"/>
        <source>Parameter%1&apos;s name must not be empty!</source>
        <translation>参数%1的名字不能为空！</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="271"/>
        <source>Invalid parameter name: %1.</source>
        <translation>无效参数名：%1.</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="275"/>
        <source>Dumplicated parameter name: %1.</source>
        <translation>参数名重复：%1.</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="286"/>
        <source>Port%1&apos;s name must not be empty!</source>
        <translation>端口%1的名字不能为空！</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="292"/>
        <source>Invalid port name: &quot;%1&quot;.</source>
        <translation>无效端口名：%1</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="296"/>
        <source>Dumplicated port name: &quot;%1&quot;.</source>
        <translation>端口名重复：%1.</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="302"/>
        <source>%1 port &quot;%2&quot; cannot be %3.</source>
        <translation>%1端口%2不能为%3.</translation>
    </message>
</context>
<context>
    <name>NewSourceWizard</name>
    <message>
        <location filename="newsourcewizard.cpp" line="17"/>
        <location filename="newsourcewizard.cpp" line="37"/>
        <location filename="newsourcewizard.cpp" line="44"/>
        <source>New Source Wizard</source>
        <translation>新建源文件向导</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="37"/>
        <source>Source &quot;%1&quot; is already existed, replace it?</source>
        <translation>源文件：&quot;%1&quot;已经存在，是否替换它？</translation>
    </message>
    <message>
        <location filename="newsourcewizard.cpp" line="44"/>
        <source>Cannot create %1</source>
        <translation>无法创建源文件：%1</translation>
    </message>
</context>
<context>
    <name>PreferenceWidget</name>
    <message>
        <location filename="preferencewidget.cpp" line="20"/>
        <source>Categories:</source>
        <translation>分类：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="26"/>
        <source>Compile Tools</source>
        <translation>编译工具</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="26"/>
        <source>Fonts</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="26"/>
        <source>Languages</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="61"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="62"/>
        <source>Setting has been modified. Save changes?</source>
        <translation>设置未保存，是否保存？</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="93"/>
        <source>Preference</source>
        <translation>首选项</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="103"/>
        <source>Iverilog Tool Path:</source>
        <translation>iverilog工具路径：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="103"/>
        <location filename="preferencewidget.cpp" line="114"/>
        <location filename="preferencewidget.cpp" line="134"/>
        <source>Browse</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="106"/>
        <source>Select Iverilog Tool Path</source>
        <translation>选择iVerilog工具路径</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="114"/>
        <source>GTKWave Tool Path:</source>
        <translation>GTKWave工具路径：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="117"/>
        <source>select wave tool path</source>
        <translation>选择波形工具路径</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="125"/>
        <source>enable log output</source>
        <translation>日志输出</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="127"/>
        <source>load hierarchy when compilation finished</source>
        <translation>编译完成时载入工程模块层次</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="134"/>
        <source>iverilog command file path(-c&lt;cmdfile&gt;):</source>
        <translation>iverilog命令文件路径(-c&lt;cmdfile&gt;):</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="137"/>
        <source>select command file</source>
        <translation>选择命令文件</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="145"/>
        <source>iverilog debug flags(-d&lt;flags&gt;):</source>
        <translation>iverilog调试标记(-d&lt;flags&gt;):</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="157"/>
        <source>iverilog generation flags(-g&lt;flags&gt;):</source>
        <translation>iverilog代际标记(-g&lt;flags&gt;):</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="170"/>
        <source>ignore missing modules(-i)</source>
        <translation>忽略未找到的模块(-i)</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="173"/>
        <source>VPI module path(-L&lt;path&gt;):</source>
        <translation>VPI模块路径(-L&lt;path&gt;)：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="174"/>
        <location filename="preferencewidget.cpp" line="188"/>
        <location filename="preferencewidget.cpp" line="202"/>
        <location filename="preferencewidget.cpp" line="225"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="175"/>
        <location filename="preferencewidget.cpp" line="189"/>
        <location filename="preferencewidget.cpp" line="203"/>
        <location filename="preferencewidget.cpp" line="226"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="177"/>
        <source>select VPI module path</source>
        <translation>选择VPI模块路径</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="187"/>
        <source>library file(-I&lt;path&gt;):</source>
        <translation>库文件(-I&lt;path&gt;):</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="191"/>
        <source>select library files</source>
        <translation>选择库文件</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="201"/>
        <source>libraries path(-y&lt;path&gt;):</source>
        <translation>库路径(-y&lt;path&gt;):</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="205"/>
        <source>select library paths</source>
        <translation>选择库路径</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="215"/>
        <source>output file name(-o&lt;file&gt;)(&apos;*&apos; will be replaced by input file name):</source>
        <translation>输出文件名(-o&lt;file&gt;)(*号代表输入文件名）</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="218"/>
        <source>set macros(-D&lt;macro&gt;):</source>
        <translation>设置宏(-D&lt;macro&gt;)：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="222"/>
        <source>Macro</source>
        <translation>宏</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="223"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="247"/>
        <source>Set VVP Extra Arguments:</source>
        <translation>设置VVP额外参数：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="304"/>
        <source>set Font:</source>
        <translation>设置字体：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="309"/>
        <source>set tab width:</source>
        <translation>设置制表符宽度：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="315"/>
        <source>set keyword color:</source>
        <translation>设置关键词颜色：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="319"/>
        <source>set quotation color:</source>
        <translation>设置引用颜色：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="323"/>
        <source>set number color:</source>
        <translation>设置数字颜色：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="327"/>
        <source>set comment color:</source>
        <translation>设置注释颜色</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="331"/>
        <source>set system function color:</source>
        <translation>设置系统函数颜色：</translation>
    </message>
    <message>
        <location filename="preferencewidget.cpp" line="355"/>
        <source>Select Language (Needs restart):</source>
        <translation>选择语言（重启后生效）：</translation>
    </message>
</context>
<context>
    <name>ProjectPropertyWidget</name>
    <message>
        <location filename="projectpropertywidget.cpp" line="23"/>
        <source>Project Name:</source>
        <translation>工程名：</translation>
    </message>
    <message>
        <location filename="projectpropertywidget.cpp" line="26"/>
        <source>Project Path:</source>
        <translation>工程路径：</translation>
    </message>
    <message>
        <location filename="projectpropertywidget.cpp" line="30"/>
        <source>Project Comment:</source>
        <oldsource>Project Comment</oldsource>
        <translation>工程注释：</translation>
    </message>
    <message>
        <location filename="projectpropertywidget.cpp" line="56"/>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="projectpropertywidget.cpp" line="56"/>
        <source>Sources</source>
        <translation>源文件</translation>
    </message>
    <message>
        <location filename="projectpropertywidget.cpp" line="69"/>
        <source>Project Property</source>
        <translation>工程属性</translation>
    </message>
</context>
<context>
    <name>ProjectStructureTab</name>
    <message>
        <location filename="projectstructuretab.cpp" line="22"/>
        <source>copy name</source>
        <translation>复制名字</translation>
    </message>
    <message>
        <location filename="projectstructuretab.cpp" line="23"/>
        <source>copy name with hierarchy</source>
        <translation>复制名字（带层次）</translation>
    </message>
    <message>
        <location filename="projectstructuretab.cpp" line="24"/>
        <source>add variable to waveform</source>
        <translation>添加变量至波形</translation>
    </message>
    <message>
        <location filename="projectstructuretab.cpp" line="70"/>
        <source>Select Top Level:</source>
        <translation>选择顶层模块：</translation>
    </message>
    <message>
        <location filename="projectstructuretab.cpp" line="73"/>
        <source>Variables:</source>
        <translation>变量：</translation>
    </message>
</context>
<context>
    <name>ProjectTabWidget</name>
    <message>
        <location filename="projecttabwidget.cpp" line="28"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="29"/>
        <source>Structure</source>
        <translation>层次</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="180"/>
        <location filename="projecttabwidget.cpp" line="192"/>
        <location filename="projecttabwidget.cpp" line="433"/>
        <source>Add Source</source>
        <translation>添加源文件</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="180"/>
        <source>File &quot;%1&quot; is already in project.
Redirect to &quot;%2&quot;?</source>
        <translation>源文件：&quot;%1&quot;已经在工程中，是否重定向？</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="192"/>
        <source>Cannot copy
%1
to
%2.
Source will remain in its own directory</source>
        <translation>无法从
%1
复制到
%2
源文件会保留在原来的目录中</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="237"/>
        <source>Select Project File</source>
        <translation>选择工程文件</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="283"/>
        <source>Add Sources</source>
        <translation>添加源文件</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="418"/>
        <source>New Project</source>
        <translation>新建工程</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="421"/>
        <source>Open Project</source>
        <translation>打开工程</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="424"/>
        <source>Close Project</source>
        <translation>关闭工程</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="427"/>
        <source>Property..</source>
        <translation>属性..</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="430"/>
        <source>New Source</source>
        <translation>新建源文件</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="436"/>
        <source>Open Source</source>
        <translation>打开源文件</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="439"/>
        <source>Remove Source</source>
        <translation>移除源文件</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="442"/>
        <source>Compile</source>
        <translation>编译</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="445"/>
        <source>Compile All</source>
        <translation>编译全部文件</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="448"/>
        <source>Compile Project</source>
        <translation>编译整个工程</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="451"/>
        <source>Stop Compile</source>
        <translation>停止编译</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="454"/>
        <source>Run</source>
        <translation>运行</translation>
    </message>
    <message>
        <location filename="projecttabwidget.cpp" line="457"/>
        <source>Show Wave</source>
        <translation>显示波形</translation>
    </message>
</context>
<context>
    <name>ShowColorLineEdit</name>
    <message>
        <location filename="showcolorlineedit.cpp" line="55"/>
        <source>Set Color</source>
        <translation>设置颜色</translation>
    </message>
</context>
<context>
    <name>SignalConnection</name>
    <message>
        <source>Compiling...</source>
        <translation type="vanished">编译中……</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="statusbar.cpp" line="14"/>
        <source>Line:    Column:    TabWidth:    </source>
        <translation>行：    列：    制表符宽度：    </translation>
    </message>
    <message>
        <location filename="statusbar.cpp" line="26"/>
        <location filename="statusbar.cpp" line="32"/>
        <source>Compiling...</source>
        <translation>编译中……</translation>
    </message>
    <message>
        <location filename="statusbar.cpp" line="50"/>
        <source>Line:%1  Column:%2  TabWidth:%3  </source>
        <translation>行：%1  列：%2  制表符宽度：%3  </translation>
    </message>
</context>
</TS>
