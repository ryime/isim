/********************************************************************************
  * @file    actionteam.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all ActionTeam functions.
  *******************************************************************************/

#include "actionteam.h"

ActionTeam::ActionTeam()
{

}

void ActionTeam::setEnabled(bool enable)
{
    for (QAction *action : *this) {
        action->setEnabled(enable);
    }
}

void ActionTeam::tempDisable()
{
    // 将tempStates的长度设置成与this相同
    int len = length();
    tempStates.resize(len);

    for (int i = 0; i < len; ++i) {
        // 存储action状态
        tempStates[i] = at(i)->isEnabled();

        value(i)->setEnabled(false);
    }
}

void ActionTeam::restore()
{
    int len = qMin(tempStates.length(), length());
    for (int i = 0; i < len; ++i) {
        value(i)->setEnabled(tempStates[i]);
    }
}
