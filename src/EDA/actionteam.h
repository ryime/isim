/********************************************************************************
  * @file    actionteam.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all ActionTeam functions.
  *******************************************************************************/

#ifndef ACTIONTEAM_H
#define ACTIONTEAM_H

#include <QObject>
#include <QAction>

/************
  * ActionTeam类，为按钮分组，组内的action可以同时启用或同时禁用.
  * （不用QActionGroup类的原因：每一个QAction只能存在于一个QActionGroup中）.
  * （注意：该ActionTeam必须在初始化时就给定action列表，之后不可更改action列表，
  *        只能对该ActionTeam进行启用、禁用操作。）
  ***********/
class ActionTeam : public QList<QAction *>
{

public:
    explicit ActionTeam();

    // 设置组内所有action启用或禁用.
    // enable为true则启用，false则禁用
    void setEnabled(bool enable);

    // 暂时禁用组内所有action
    void tempDisable();

    // 恢复组内所有action原先的状态
    void restore();

private:
    // 存储组内action的状态，以便恢复
    QList<bool> tempStates;
};

#endif // ACTIONTEAM_H
