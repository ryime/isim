/********************************************************************************
  * @file    codeeditor.cpp
  * @author  Lun Li
  * @version V2.2.3
  * @date    2022.9.14
  * @brief   This file provides all CodeEditor functions.
  *******************************************************************************/

#include "codeeditor.h"
#include <QAbstractItemView>

#include <QMimeData>
#include <QPainter>
#include <QTextBlock>

CodeEditor::CodeEditor(ProgramConfig *const config,
                       Source *file,
                       QMenu *menu,
                       QWidget *parent)
    : QPlainTextEdit(parent),
      contextMenu(menu),
      m_file(file),
      m_config(config)
{
    //初始化行号区域与语法高亮器
    lineNumberArea = new LineNumberArea(this);    
    new VerilogSyntaxHighlighter(config, document());

    // 连接信号槽
    connect(this, &CodeEditor::blockCountChanged, this, &CodeEditor::updateLineNumberAreaWidth);
    connect(this, &CodeEditor::updateRequest, this, &CodeEditor::updateLineNumberArea);
    connect(this, &CodeEditor::cursorPositionChanged, this, &CodeEditor::highlightCurrentLine);    
    connect(m_config, &ProgramConfig::editorConfigChanged, this, &CodeEditor::updateEditorConfig);

    // 设置补全器
    m_completer = new VerilogCompleter(this);
    m_completer->setWidget(this);
    connect(m_completer, QOverload<const QString &>::of(&QCompleter::activated), this, [=](const QString &text) {
        QTextCursor cursor = textCursor();
        cursor.select(QTextCursor::WordUnderCursor);
        cursor.insertText(text);
    });

    setLineWrapMode(NoWrap);
    updateLineNumberAreaWidth(0);
    highlightCurrentLine();    
    updateEditorConfig();    
}

CodeEditor::~CodeEditor()
{
    // 若该文件不在工程中，则关闭编辑器时就删除文件指针
    if (!m_file->projectItem()) {
        m_file->deleteLater();
        m_file = nullptr;
    }
}

int CodeEditor::lineNumberAreaWidth()
{
    // 计算行数的位数
    int digits = 1;
    int max = qMax(1, blockCount());
    while (max >= 10) {
        max /= 10;
        ++digits;
    }

    // 根据行号的位数计算宽度
    int space = 3 + fontMetrics().horizontalAdvance(QLatin1Char('9')) * digits;

    return space;
}

void CodeEditor::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void CodeEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy) { // 行号随文本一起滚动
        lineNumberArea->scroll(0, dy);
    } else { // 更新行号区域
        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());
    }

    if (rect.contains(viewport()->rect())) {
        updateLineNumberAreaWidth(0);
    }
}

void CodeEditor::updateEditorConfig()
{
    setFont(m_config->font());
    setTabStopDistance(m_config->tabWidth() * QFontMetrics(m_config->font()).horizontalAdvance(" "));
}

Source *CodeEditor::file() const
{
    return m_file;
}

void CodeEditor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void CodeEditor::focusInEvent(QFocusEvent *e)
{
    QPlainTextEdit::focusInEvent(e);
    emit focusChanged(true);
}

void CodeEditor::focusOutEvent(QFocusEvent *e)
{
    if (e->reason() != Qt::FocusReason::PopupFocusReason) {
        QPlainTextEdit::focusOutEvent(e);
        emit focusChanged(false);
    } else {
        e->ignore(); // 点菜单则不失去焦点
    }
}

void CodeEditor::contextMenuEvent(QContextMenuEvent *e)
{
    // 弹出菜单
    contextMenu->exec(e->globalPos());
}

void CodeEditor::keyPressEvent(QKeyEvent *event)
{
    if (m_completer) {
        if (m_completer->popup()->isVisible()) {
            switch(event->key()) {
            case Qt::Key_Escape:
            case Qt::Key_Enter:
            case Qt::Key_Return:
            case Qt::Key_Tab:
                event->ignore();
                return;
            default:
                break;
            }
        }

        // 换行时自动缩进
        if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
            CodeTextBlockUserData *blockData = static_cast<CodeTextBlockUserData *>(textCursor().block().userData());
            if (blockData) {
                if (blockData->endScopeKeywordNum > 0) {
                    QString text = textCursor().block().text();
                    while (text.at(0) == '\t' || text.at(0) == ' ') {
                        text.remove(0, 1);
                    }
                    text.prepend(QString(blockData->indentStack - blockData->startScopeKeywordNum, '\t'));
                    QTextCursor cursor = textCursor();
                    cursor.select(QTextCursor::LineUnderCursor);
                    cursor.removeSelectedText();
                    cursor.insertText(text);
                }
                QPlainTextEdit::keyPressEvent(event);
                textCursor().insertText(QString(blockData->indentStack, '\t'));
            } else {
                QPlainTextEdit::keyPressEvent(event);
            }
        } else { // 弹出补全器
            QPlainTextEdit::keyPressEvent(event);
            switch(event->key()) {
            case Qt::Key_Up:
            case Qt::Key_Down:
            case Qt::Key_Left:
            case Qt::Key_Right:
                m_completer->popup()->setVisible(false);
                return;
            default:
                break;
            }
            QTextCursor cursor = textCursor();
            cursor.select(QTextCursor::WordUnderCursor);
            QString prefix = cursor.selectedText();
            if (prefix.length() > 1) {
                m_completer->setCompletionPrefix(prefix);
                cursor.clearSelection();
                cursor.movePosition(QTextCursor::Left, QTextCursor::MoveAnchor, prefix.length());                                                                                                                                                                                      QRect rect = cursorRect(cursor);
                rect.setWidth(200);
                rect.setTopLeft(rect.bottomLeft());
                m_completer->complete(rect);
                QItemSelectionModel* sm = new QItemSelectionModel(m_completer->completionModel());
                m_completer->popup()->setSelectionModel(sm);
                sm->setCurrentIndex(m_completer->popup()->model()->index(0,0), QItemSelectionModel::Current);
                sm->select(sm->currentIndex(), QItemSelectionModel::Select);
            } else {
                m_completer->popup()->setVisible(false);
            }
        }
    }
}

void CodeEditor::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;

        QColor lineColor = QColor(Qt::yellow).lighter(160);

        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    setExtraSelections(extraSelections);
}

void CodeEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(lineNumberArea);
    painter.fillRect(event->rect(), Qt::lightGray);

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = qRound(blockBoundingGeometry(block).translated(contentOffset()).top());
    int bottom = top + qRound(blockBoundingRect(block).height());

    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::black);
            painter.drawText(0, top, lineNumberArea->width(), fontMetrics().height(),
                             Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + qRound(blockBoundingRect(block).height());
        ++blockNumber;
    }
}



