/********************************************************************************
  * @file    codetextblockuserdata.h
  * @author  Lun Li
  * @version V2.2.3
  * @date    2022.9.14
  * @brief   This file contains all the functions prototypes for the CodeTextBlockUserData.
  ******************************************************************************/

#ifndef CODETEXTBLOCKUSERDATA_H
#define CODETEXTBLOCKUSERDATA_H

#include <QTextBlockUserData>
#include <QObject>

/************
  * CodeTextBlockUserData结构，用于存储每一个文本块内的缩进相关变量
  ***********/
struct CodeTextBlockUserData : public QTextBlockUserData
{
    CodeTextBlockUserData();
    // 缩进
    int indentStack = 0;

    // 该行范围起始关键字的数目
    int startScopeKeywordNum = 0;

    // 该行范围结束关键字的数目
    int endScopeKeywordNum = 0;
};

#endif // CODETEXTBLOCKUSERDATA_H
