/********************************************************************************
  * @file    comboboxitemdelegate.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for the ComboBoxItemDelegate.
  ******************************************************************************/

#ifndef COMBOBOXITEMDELEGATE_H
#define COMBOBOXITEMDELEGATE_H

#include <QStyledItemDelegate>
#include <QComboBox>

/************
  * ComboBoxItemDelegate类，用于给单元格提供一个下拉列表（QComboBox）
  ***********/
class ComboBoxItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    // strList: 下拉列表
    explicit ComboBoxItemDelegate(const QStringList &strList,
                                  QObject *parent = nullptr);

    // 创建ComboBox
    QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;

    // 设置ComboBox的内容
    void setEditorData(QWidget *editor,
                       const QModelIndex &index) const override;

    // 通过ComboBox设置model的内容
    void setModelData(QWidget *editor,
                      QAbstractItemModel *model,
                      const QModelIndex &index) const override;

private:
    // 下拉列表中的字符串列表
    QStringList m_strList;
};

#endif // COMBOBOXITEMDELEGATE_H
