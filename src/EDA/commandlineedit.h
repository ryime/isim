/********************************************************************************
  * @file    commandlineedit.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for the CommandLineEdit.
  ******************************************************************************/

#ifndef COMMANDLINEEDIT_H
#define COMMANDLINEEDIT_H

#include <QLineEdit>
#include <QQueue>

/************
  * CommandLineEdit类，用于给用户提供命令行输入框，并记录已输入的命令
  ***********/
class CommandLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit CommandLineEdit(QWidget *parent = nullptr);

    // 向命令队列添加命令
    // command: 命令字符串
    void addCommand(const QString &command);

    // 清空当前命令
    void clear();

protected:
    // 支持上键和下键浏览已输入命令
    void keyPressEvent(QKeyEvent *event) override;

private:
    QQueue<QString> commandList;
    int ptr;
    QString currentCommand;
};

#endif // COMMANDLINEEDIT_H
