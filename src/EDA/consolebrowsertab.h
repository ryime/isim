/********************************************************************************
  * @file    consolebrowsertab.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for ConsoleBrowserTab.
  ******************************************************************************/


#ifndef CONSOLEBROWSERTAB_H
#define CONSOLEBROWSERTAB_H

#include <QPlainTextEdit>
#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QScrollBar>
#include "nomarginlayout.h"
#include "programconfig.h"
#include "commandlineedit.h"

/************
  * ConsoleBrowserTab类，用于命令行的显示。
  ***********/
class ConsoleBrowserTab : public QWidget
{
    Q_OBJECT

public:
    explicit ConsoleBrowserTab(ProgramConfig *const config, QWidget *parent = nullptr);

    // 添加命令行输入的显示
    void appendInput(const QString &cmd);

    // 添加命令行输出的显示
    void appendOutput(const QString &out);

    // 设置命令行输入框是否可用
    void setUsable(bool usable);

signals:
    // 当命令行输入框完成输入（用户敲回车）后，发出该信号
    void cmdIsInput(const QString &cmd);

private slots:
    // 当命令行输入框完成输入（用户敲回车）后，清空输入框并显示命令
    void on_cmdInputReturnPressed();

    // 当用户导入命令行文件后，执行该函数
    void on_cmdFileButtonClicked();

private:
    QPlainTextEdit *hintView;
    QPlainTextEdit *consoleView;
    CommandLineEdit *cmdInputLineEdit;
    QPushButton *cmdFileButton;
    ProgramConfig *const m_config;
};

#endif // CONSOLEBROWSERTAB_H
