/********************************************************************************
  * @file    consoleerrortab.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all ConsoleErrorTab functions.
  ******************************************************************************/

#include "consoleerrortab.h"
#include "signalconnection.h"

ConsoleErrorTab::ConsoleErrorTab(QWidget *parent)
    : QListView(parent)
{
    setWordWrap(true);
    setEditTriggers(QListView::NoEditTriggers);
    m_model = new QStandardItemModel(this);
    setModel(m_model);

    extern SignalConnection globalSignal;
    connect(this, &QListView::doubleClicked, this, [=](const QModelIndex &index) {
        emit globalSignal.errorItemClicked(m_model->data(index, Qt::UserRole).toString(),
                                           m_model->data(index, Qt::UserRole + 1).toInt());
    });
}

void ConsoleErrorTab::updateErrorList(const QString &err)
{
    // 检测错误信息
    static QRegularExpression re(R"((.*?):(\d+):.*)");
    QRegularExpressionMatchIterator i = re.globalMatch(err);
    while (i.hasNext()) {
        QRegularExpressionMatch match = i.next();
        QString filePath = QDir::fromNativeSeparators(match.captured(1));
        int lineNumber = match.captured(2).toInt();
        QStandardItem *item = new QStandardItem(match.captured(0));
        item->setData(filePath, Qt::UserRole);
        item->setData(lineNumber, Qt::UserRole + 1);
        m_model->appendRow(item);
    }
    this->show();
}

QStandardItemModel *ConsoleErrorTab::model() const
{
    return m_model;
}
