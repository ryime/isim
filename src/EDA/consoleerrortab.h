/********************************************************************************
  * @file    consoleerrortab.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for ConsoleErrorTab.
  ******************************************************************************/

#ifndef CONSOLEERRORTAB_H
#define CONSOLEERRORTAB_H

#include <QListView>
#include <QStandardItemModel>
#include <QDir>

/************
  * ConsoleErrorTab类，用于显示命令行产生的错误。
  ***********/
class ConsoleErrorTab : public QListView
{
    Q_OBJECT
public:
    explicit ConsoleErrorTab(QWidget *parent = nullptr);

    // 根据错误字符串，更新错误列表
    void updateErrorList(const QString &err);

    // 获取该view对应的model
    QStandardItemModel *model() const;

private:
    QStandardItemModel *m_model;
};

#endif // CONSOLEERRORTAB_H
