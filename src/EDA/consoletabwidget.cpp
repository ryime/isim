/********************************************************************************
  * @file    consoletabwidget.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all ConsoleTabWidget functions.
  ******************************************************************************/

#include "consoletabwidget.h"

ConsoleTabWidget::ConsoleTabWidget(ProgramConfig *const config, QWidget *parent)
    :QTabWidget(parent)
{
    setTabPosition(QTabWidget::South);
    consoleCompile = new ConsoleBrowserTab(config);
    consoleWave = new ConsoleBrowserTab(config);
    consoleError = new ConsoleErrorTab;
    addTab(consoleCompile, tr("Compile"));
    addTab(consoleWave, tr("Wave"));
    addTab(consoleError, tr("Error"));

    connect(consoleCompile, &ConsoleBrowserTab::cmdIsInput, this, &ConsoleTabWidget::compileCmdIsInput);
    connect(consoleWave, &ConsoleBrowserTab::cmdIsInput, this, &ConsoleTabWidget::waveCmdIsInput);

    extern SignalConnection globalSignal;
    connect(&globalSignal, &SignalConnection::compileInput, this, &ConsoleTabWidget::appendCompileInput);
    connect(&globalSignal, &SignalConnection::compileOutput, this, &ConsoleTabWidget::appendCompileOutput);
    connect(&globalSignal, &SignalConnection::compileHint, this, &ConsoleTabWidget::appendCompileHint);
    connect(&globalSignal, &SignalConnection::compileError, this, &ConsoleTabWidget::appendCompileError);
    connect(&globalSignal, &SignalConnection::waveInput, this, &ConsoleTabWidget::appendWaveInput);
    connect(&globalSignal, &SignalConnection::waveOutput, this, &ConsoleTabWidget::appendWaveOutput);
    connect(&globalSignal, &SignalConnection::waveError, this, &ConsoleTabWidget::appendWaveError);
    connect(&globalSignal, &SignalConnection::projectOpened, this, [&] {
        setUsable(true);
    });
    connect(&globalSignal, &SignalConnection::projectClosed, this, [&] {
        setUsable(false);
    });
    connect(&globalSignal, &SignalConnection::compileStarted, this, [=] {
        resetError();
    });
    connect(&globalSignal, &SignalConnection::VVPStarted, this, [=] {
        resetError();
    });

    setUsable(false);
}

void ConsoleTabWidget::setUsable(bool usable)
{
    consoleCompile->setUsable(usable);
    consoleWave->setUsable(usable);
    if (!usable) {
        consoleError->model()->clear();
    }
}

void ConsoleTabWidget::resetError()
{
    consoleError->model()->clear();
}

void ConsoleTabWidget::appendCompileInput(const QString &cmd)
{
    consoleCompile->appendInput(cmd);
    setCurrentIndex(Tabs::Compile);
}

void ConsoleTabWidget::appendCompileOutput(const QString &cmd)
{
    consoleCompile->appendOutput(cmd);
    setCurrentIndex(Tabs::Compile);
}

void ConsoleTabWidget::appendCompileHint(const QString &hint)
{
    consoleCompile->appendOutput(hint);
    setCurrentIndex(Tabs::Compile);
}

void ConsoleTabWidget::appendWaveInput(const QString &cmd)
{
    consoleWave->appendInput(cmd);
    setCurrentIndex(Tabs::Wave);
}

void ConsoleTabWidget::appendWaveOutput(const QString &cmd)
{
    consoleWave->appendOutput(cmd);
    setCurrentIndex(Tabs::Wave);
}

void ConsoleTabWidget::appendCompileError(const QString &err)
{
    consoleCompile->appendOutput(err);
    setCurrentIndex(Tabs::Compile);
    consoleError->updateErrorList(err);
}

void ConsoleTabWidget::appendWaveError(const QString &err)
{
    consoleWave->appendOutput(err);
    setCurrentIndex(Tabs::Wave);
    consoleError->updateErrorList(err);
}
