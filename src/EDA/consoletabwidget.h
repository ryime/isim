/********************************************************************************
  * @file    consoletabwidget.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for ConsoleTabWidget.
  ******************************************************************************/

#ifndef CONSOLETABWIDGET_H
#define CONSOLETABWIDGET_H

#include <QTabWidget>
#include <QWidget>
#include <QRegularExpression>
#include <QRegularExpressionMatchIterator>
#include <QListView>
#include <QStringListModel>
#include <QFileInfo>
#include "consolebrowsertab.h"
#include "consoleerrortab.h"
#include "signalconnection.h"

/************
  * ConsoleTabWidget类，是一个标签窗口，用于显示命令有关的信息。
  * 该窗口包含三个标签，Verilog编译命令行窗口(Compile)，波形命令行窗口(Wave)和
  * 错误显示窗口(Error)。
  ***********/
class ConsoleTabWidget : public QTabWidget
{
    Q_OBJECT

public:
    // 标签的枚举值
    enum Tabs {
        Compile, Wave, Error
    };

    explicit ConsoleTabWidget(ProgramConfig *const config, QWidget *parent = nullptr);

    // 设置命令行输入框是否可用
    void setUsable(bool usable);

    // 清空错误列表
    void resetError();

public slots:
    // 添加Verilog编译的命令行输入
    void appendCompileInput(const QString &cmd);

    // 添加Verilog编译的命令行输出
    void appendCompileOutput(const QString &cmd);

    // 添加Verilog编译的命令行提示
    void appendCompileHint(const QString &hint);

    // 添加波形的命令行输入
    void appendWaveInput(const QString &cmd);

    // 添加波形的命令行输出
    void appendWaveOutput(const QString &cmd);

    // 添加Verilog编译的错误输出
    void appendCompileError(const QString &err);

    // 添加波形的命令行错误输出
    void appendWaveError(const QString &err);

signals:
    // 需要转到该文件对应行
    void fileLineNeedTurned(const QString &filePath, int lineNumber);

    // 当编译命令行输入框完成输入（用户敲回车）后，发出该信号
    void compileCmdIsInput(const QString &cmd);

    // 当波形命令行输入框完成输入（用户敲回车）后，发出该信号
    void waveCmdIsInput(const QString &cmd);

private:
    ConsoleBrowserTab *consoleCompile;
    ConsoleBrowserTab *consoleWave;
    ConsoleErrorTab *consoleError;
};

#endif // CONSOLETABWIDGET_H
