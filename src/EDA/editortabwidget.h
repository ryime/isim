/********************************************************************************
  * @file    editortabwidget.h
  * @author  Lun Li
  * @version V2.2.1
  * @date    2022.9.2
  * @brief   This file contains all the functions prototypes for EditorTabWidget.
  ******************************************************************************/

#ifndef EDITORTABWIDGET_H
#define EDITORTABWIDGET_H

#include <QAction>
#include <QActionGroup>
#include <QTabWidget>
#include "codeeditor.h"
#include "programconfig.h"
#include "source.h"
#include "actionteam.h"

class FindWidget;

/************
  * EditorTabWidget类，是一个标签窗口，每一个标签都是一个代码编辑器。
  ***********/
class EditorTabWidget : public QTabWidget
{
    Q_OBJECT
public:
    // 按钮的枚举值
    enum Actions {
        NewFile, OpenFile, CloseFile, SaveFile, SaveAsFile, SaveAllFile,
        Cut, Copy, Paste, Undo, Redo, Delete, SelectAll, FindAndReplace,
        ZoomIn, ZoomOut
    };

    explicit EditorTabWidget(ProgramConfig *const config, QWidget *parent = nullptr);

    // 获取该类所控制的按钮列表
    const QList<QAction *> &actionList() const;

    // 获取第index个编辑器
    CodeEditor *widget(int index) const;

    // 获取查找窗口
    FindWidget *findWidget() const;

public slots:
    // 打开一个文件
    void openSourceFile(Source *file);

    // 关闭一个文件
    void closeSourceFile(Source *file);

    // 保存一个文件
    void saveSourceFile(Source *file, const QString &content = QString());

    // 各个按钮按下时对应的槽函数
    void on_newFileButtonClicked();
    void on_openFileButtonClicked();
    void on_closeFileButtonClicked();
    void on_saveFileButtonClicked();
    void on_saveAsFileButtonClicked();
    void on_saveAllFileButtonClicked();
    void on_cutButtonClicked();
    void on_copyButtonClicked();
    void on_pasteButtonClicked();
    void on_undoButtonClicked();
    void on_redoButtonClicked();
    void on_deleteButtonClicked();
    void on_selectAllButtonClicked();
    void on_findAndReplaceButtonClicked();
    void on_zoomInButtonClicked();
    void on_zoomOutButtonClicked();

signals:
    void cursorPositionChanged(int line, int column);

private:
    ProgramConfig *const m_config;
    QMenu *contextMenu;
    FindWidget *m_findWidget;
    QList<QAction *> m_actionList;
    ActionTeam editorActionTeam;

    void initializeActionList();

    int newFileCount = 0;

private slots:
    // 当前标签切换时，执行该函数
    void on_currentChanged(int index);

    // 当前标签被拖拽移动时，执行该函数
    void on_tabMoved(int from, int to);

    // 当前标签要关闭时，执行该函数
    void on_tabCloseRequested(int index);

};

/************
  * FindWidget类，用于在编辑器中提供查找和替换功能。
  ***********/
class FindWidget : public QWidget
{
    Q_OBJECT

public:
    enum FindType {
        NextToEnd, PreviousLoop, NextLoop
    };
    explicit FindWidget(EditorTabWidget *editorTabWidget);

public slots:
    void show();
    void replace();
    bool find(FindWidget::FindType type);

protected:
    QSize sizeHint() const override;
    void keyPressEvent(QKeyEvent *event) override;

private:
    EditorTabWidget *editorTabWidget;

    QPushButton *findPreviousButton;
    QPushButton *findNextButton;
    QCheckBox *regexpCheckBox;
    QCheckBox *caseCheckBox;
    QLineEdit *expInput;
    QLineEdit *replaceExpInput;

    void setExpRed();
};

#endif // EDITORTABWIDGET_H
