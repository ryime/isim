/********************************************************************************
  * @file    hierarchyitem.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all HierarchyItem functions.
  ******************************************************************************/

#include "hierarchyitem.h"

HierarchyItem::HierarchyItem(const QString &instanceName,
                             const QString &definitionName,
                             Source *file)
    : QStandardItem(),
      m_instanceName(instanceName),
      m_definitionName(definitionName),
      m_file(file)
{
    if (file) {
        setText(instanceName + " -- " + definitionName + " (" + file->fileName() + ")");
    } else {
        setText(instanceName + " -- " + definitionName);
    }
    setIcon(QIcon(":/icon/CorFile.png"));
}

HierarchyItem::~HierarchyItem()
{
    m_variableModel->deleteLater();
}

HierarchyItem *HierarchyItem::parent() const
{
    return (HierarchyItem *)(QStandardItem::parent());
}

HierarchyItem *HierarchyItem::child(int row) const
{
    return (HierarchyItem *)(QStandardItem::child(row));
}

Source *HierarchyItem::file() const
{
    return m_file;
}

void HierarchyItem::setFile(Source *newFile)
{
    m_file = newFile;
}

QStandardItemModel *HierarchyItem::variableModel() const
{
    return m_variableModel;
}

void HierarchyItem::setVariableModel(QStandardItemModel *newVariableModel)
{
    m_variableModel = newVariableModel;
}

const QString &HierarchyItem::instanceName() const
{
    return m_instanceName;
}

void HierarchyItem::setInstanceName(const QString &newInstanceName)
{
    m_instanceName = newInstanceName;
}

const QString &HierarchyItem::definitionName() const
{
    return m_definitionName;
}

void HierarchyItem::setDefinitionName(const QString &newDefinitionName)
{
    m_definitionName = newDefinitionName;
}
