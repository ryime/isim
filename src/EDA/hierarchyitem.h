/********************************************************************************
  * @file    hierarchyitem.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for HierarchyItem.
  ******************************************************************************/

#ifndef HIERARCHYITEM_H
#define HIERARCHYITEM_H

#include <QStandardItem>
#include <QObject>
#include <QStandardItemModel>
#include "source.h"

/************
  * HierarchyItem类，是一个模型项，代表了Verilog层次结构中的每一项
  ***********/
class HierarchyItem : public QStandardItem
{
public:
    explicit HierarchyItem(const QString &instanceName, const QString &definitionName, Source *file = nullptr);
    ~HierarchyItem();

    // 该项的父亲
    HierarchyItem *parent() const;

    // 该项的第row个孩子
    HierarchyItem *child(int row) const;

    // 该项对应的文件
    Source *file() const;
    void setFile(Source *newFile);

    // 该项对应的变量列表
    QStandardItemModel *variableModel() const;
    void setVariableModel(QStandardItemModel *newVariableModel);

    // 该项的实例名字
    const QString &instanceName() const;
    void setInstanceName(const QString &newInstanceName);

    // 该项的定义名字
    const QString &definitionName() const;
    void setDefinitionName(const QString &newDefinitionName);

private:
    QStandardItemModel *m_variableModel;
    QString m_instanceName;
    QString m_definitionName;
    Source *m_file;
};

#endif // HIERARCHYITEM_H
