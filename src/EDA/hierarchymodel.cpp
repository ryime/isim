/********************************************************************************
  * @file    hierarchymodel.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all HierarchyModel functions.
  ******************************************************************************/

#include "hierarchymodel.h"

HierarchyModel::HierarchyModel(const QString &name, Source *file)
    : QStandardItemModel{}, m_name(name), m_file(file)
{
}

bool HierarchyModel::operator ==(const HierarchyModel &other)
{
    return m_name == other.m_name;
}

HierarchyItem *HierarchyModel::itemFromIndex(const QModelIndex &index) const
{
    return (HierarchyItem *)(QStandardItemModel::itemFromIndex(index));
}

HierarchyItem *HierarchyModel::item(int row) const
{
    return (HierarchyItem *)(QStandardItemModel::item(row));
}

const QString &HierarchyModel::name() const
{
    return m_name;
}

void HierarchyModel::setName(const QString &newName)
{
    m_name = newName;
}

Source *HierarchyModel::file() const
{
    return m_file;
}

void HierarchyModel::setFile(Source *newFile)
{
    m_file = newFile;
}
