/********************************************************************************
  * @file    hierarchymodel.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for HierarchyModel.
  ******************************************************************************/

#ifndef HIERARCHYMODEL_H
#define HIERARCHYMODEL_H

#include <QStandardItemModel>
#include <QObject>
#include "hierarchyitem.h"

class ProjectItem;

/************
  * HierarchyModel类，是一个模型，代表了一整个Verilog层次结构。
  ***********/
class HierarchyModel : public QStandardItemModel
{
    Q_OBJECT
public:
    explicit HierarchyModel(const QString &name, Source *file = nullptr);

    // 依据名字来辨别两个模型是否相等
    bool operator ==(const HierarchyModel &other);

    // 获取第index处的项
    HierarchyItem *itemFromIndex(const QModelIndex &index) const;

    // 获取第row个项
    HierarchyItem *item(int row) const;

    // 获取名字
    const QString &name() const;
    void setName(const QString &newName);

    // 获取对应的文件
    Source *file() const;
    void setFile(Source *newFile);

private:
    QString m_name;
    Source *m_file;
};

#endif // HIERARCHYMODEL_H
