/********************************************************************************
  * @file    mainwindow.cpp
  * @author  Lun Li
  * @version V2.2.1
  * @date    2022.9.2
  * @brief   This file provides all the MainWindow functions.
  *******************************************************************************/

#include "consoletabwidget.h"
#include "mainwindow.h"
#include <QCoreApplication>
#include <QTranslator>

MainWindow::MainWindow(ProgramConfig *configFile, QWidget *parent)
    : QMainWindow(parent)
{
    configFile->setParent(this);
    setWindowTitle(tr("iSim"));

    // 初始化工程管理区
    projectDock = new QDockWidget(tr("Project"), this);
    projectTabWidget = new ProjectTabWidget(configFile, this);
    projectDock->setWidget(projectTabWidget);

    // 初始化代码编辑区
    editorDock = new QDockWidget(tr("Editor"), this);
    editorTabWidget = new EditorTabWidget(configFile, this);
    QWidget *editorWidget = new QWidget;
    editorWidget->setLayout(new NoMarginVBoxLayout);
    editorWidget->layout()->addWidget(editorTabWidget->findWidget());
    editorWidget->layout()->addWidget(editorTabWidget);
    editorDock->setWidget(editorWidget);

    // 初始化控制台区
    consoleDock = new QDockWidget(tr("Console"), this);
    consoleTabWidget = new ConsoleTabWidget(configFile, this);
    consoleDock->setWidget(consoleTabWidget);

    // 设置停靠窗口的位置与大小
    addDockWidget(Qt::TopDockWidgetArea, projectDock);
    addDockWidget(Qt::TopDockWidgetArea, editorDock);
    addDockWidget(Qt::BottomDockWidgetArea, consoleDock);
    splitDockWidget(projectDock, consoleDock, Qt::Vertical);
    splitDockWidget(projectDock, editorDock, Qt::Horizontal);
    resizeDocks({projectDock, editorDock},{size().width() / 5, size().width() - size().width() / 5}, Qt::Horizontal);
    resizeDocks({projectDock, consoleDock},{size().height() - size().height() / 3,size().height() / 3}, Qt::Vertical);

    // 初始化菜单栏、工具栏与状态栏
    menuBar = new MenuBar(projectTabWidget,
                          editorTabWidget,
                          QList<QDockWidget *>() << projectDock << editorDock << consoleDock,
                          configFile,
                          this);
    setMenuBar(menuBar);
    toolBar = new ToolBar(projectTabWidget, editorTabWidget, this);
    addToolBar(toolBar);
    statusBar = new StatusBar(configFile, this);
    statusBar->setEditorDetail(1, 1, configFile->tabWidth());
    setStatusBar(statusBar);

    // 2022.9.2 新功能：使用该程序直接打开文件
    QStringList argv = qApp->arguments();
    if (argv.length() > 1) {
        QFileInfo openFile(QDir::fromNativeSeparators(argv.at(1)));
        if (openFile.suffix() == "proj") {
            projectTabWidget->openProject(openFile.filePath());
        } else {
            editorTabWidget->openSourceFile(new Source(openFile.filePath(), false, this));
        }
    }
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    // 主窗口大小改变时，停靠窗口的相对大小也跟着改变
    QMainWindow::resizeEvent(event);
    resizeDocks({projectDock, editorDock},{size().width() / 5, size().width() - size().width() / 5}, Qt::Horizontal);
    resizeDocks({projectDock, consoleDock},{size().height() - size().height() / 3,size().height() / 3}, Qt::Vertical);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    // 关闭程序时，检查代码编辑器是否有未保存的文件
    int i = editorTabWidget->count();
    while (i--) {
        editorTabWidget->closeSourceFile(editorTabWidget->widget(i)->file()); // 尝试关闭
    }
    if (editorTabWidget->count() > 0) { // 有未保存的文件
        event->ignore();
    } else {
        event->accept();
    }
}

