/********************************************************************************
  * @file    mainwindow.h
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file contains all the functions prototypes for MainWindow.
  ******************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDockWidget>
#include "consoletabwidget.h"
#include "menubar.h"
#include "toolbar.h"
#include "statusbar.h"
#include "signalconnection.h"

/************
  * MainWindow类，是程序的主窗口。
  * 主要包括三个区域：工程管理区、代码编辑区、控制台区
  ***********/
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(ProgramConfig *configFile, QWidget *parent = nullptr);

protected:
    void resizeEvent(QResizeEvent *event) override;
    void closeEvent(QCloseEvent* event) override;

private:
    // 菜单栏、工具栏、状态栏
    MenuBar* menuBar;
    ToolBar* toolBar;
    StatusBar *statusBar;

    // 三个区域所处的停靠窗口
    QDockWidget* projectDock;
    QDockWidget* editorDock;
    QDockWidget* consoleDock;

    // 三个区域
    ProjectTabWidget *projectTabWidget;
    EditorTabWidget *editorTabWidget;
    ConsoleTabWidget *consoleTabWidget;

};
#endif // MAINWINDOW_H
