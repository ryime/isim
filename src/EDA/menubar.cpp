/********************************************************************************
  * @file    menubar.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all MenuBar functions.
  ******************************************************************************/

#include "menubar.h"

MenuBar::MenuBar(ProjectTabWidget *ptw,
                 EditorTabWidget *etw,
                 const QList<QDockWidget *> &docks,
                 ProgramConfig *configFile,
                 QWidget *parent)
    : QMenuBar(parent),
      m_configFile(configFile)
{
    // 文件(File)菜单
    menuFile = addMenu(tr("File"));
    menuFile->addAction(ptw->actionList().at(ProjectTabWidget::NewProject));
    menuFile->addAction(ptw->actionList().at(ProjectTabWidget::OpenProject));
    menuFile->addAction(ptw->actionList().at(ProjectTabWidget::CloseProject));
    menuFile->addSeparator();
    menuFile->addAction(etw->actionList().at(EditorTabWidget::NewFile));
    menuFile->addAction(etw->actionList().at(EditorTabWidget::OpenFile));
    menuFile->addAction(etw->actionList().at(EditorTabWidget::CloseFile));
    menuFile->addAction(etw->actionList().at(EditorTabWidget::SaveFile));
    menuFile->addAction(etw->actionList().at(EditorTabWidget::SaveAsFile));
    menuFile->addAction(etw->actionList().at(EditorTabWidget::SaveAllFile));

    // 编辑(Edit)菜单
    menuEdit = addMenu(tr("Edit"));
    menuEdit->addAction(etw->actionList().at(EditorTabWidget::Cut));
    menuEdit->addAction(etw->actionList().at(EditorTabWidget::Copy));
    menuEdit->addAction(etw->actionList().at(EditorTabWidget::Paste));
    menuEdit->addAction(etw->actionList().at(EditorTabWidget::Undo));
    menuEdit->addAction(etw->actionList().at(EditorTabWidget::Redo));
    menuEdit->addAction(etw->actionList().at(EditorTabWidget::Delete));
    menuEdit->addAction(etw->actionList().at(EditorTabWidget::SelectAll));
    menuEdit->addAction(etw->actionList().at(EditorTabWidget::FindAndReplace));

    // 视图(View)菜单
    menuView = addMenu(tr("View"));
    menuViewPanel = menuView->addMenu(tr("Panel"));
    for (QDockWidget *dock : docks){ // 加入主界面各个停靠窗口关联的按钮
        menuViewPanel->addAction(dock->toggleViewAction());
    }
    menuViewZoom = menuView->addMenu(tr("Zoom"));
    menuViewZoom->addAction(etw->actionList().at(EditorTabWidget::ZoomIn));
    menuViewZoom->addAction(etw->actionList().at(EditorTabWidget::ZoomOut));

    // 工程(Project)菜单
    menuProject = addMenu(tr("Project"));
    menuProject->addAction(ptw->actionList().at(ProjectTabWidget::NewSource));
    menuProject->addAction(ptw->actionList().at(ProjectTabWidget::AddSource));
    menuProject->addAction(ptw->actionList().at(ProjectTabWidget::Property));

    // 源文件(Source)菜单
    menuSource = addMenu(tr("Source"));
    menuSource->addAction(ptw->actionList().at(ProjectTabWidget::OpenSource));
    menuSource->addAction(ptw->actionList().at(ProjectTabWidget::RemoveSource));

    // 进程(Process)菜单
    menuProcess = addMenu(tr("Process"));
    menuProcess->addAction(ptw->actionList().at(ProjectTabWidget::Compile));
    menuProcess->addAction(ptw->actionList().at(ProjectTabWidget::CompileAll));
    menuProcess->addAction(ptw->actionList().at(ProjectTabWidget::CompileProject));
    menuProcess->addAction(ptw->actionList().at(ProjectTabWidget::Run));
    menuProcess->addAction(ptw->actionList().at(ProjectTabWidget::StopCompile));
    menuProcess->addAction(ptw->actionList().at(ProjectTabWidget::ShowWave));

    // 工具(Tools)菜单
    menuTools = addMenu(tr("Tools"));
    menuTools->addAction(tr("Preference"), this, &MenuBar::on_preferenceButtonClicked);
}

void MenuBar::on_preferenceButtonClicked()
{
    PreferenceWidget *preferWidget = new PreferenceWidget(m_configFile);
    preferWidget->setAttribute(Qt::WA_DeleteOnClose);
    preferWidget->setGeometry(QRect(static_cast<QWidget *>(parent())->width() / 4,
                                    static_cast<QWidget *>(parent())->height() / 4,
                                    static_cast<QWidget *>(parent())->width() / 2,
                                    static_cast<QWidget *>(parent())->height() / 2));
    preferWidget->show();
}
