/********************************************************************************
  * @file    menubar.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for MenuBar.
  ******************************************************************************/

#ifndef MENUBAR_H
#define MENUBAR_H

#include <QMenuBar>
#include <QDockWidget>
#include "projecttabwidget.h"
#include "editortabwidget.h"
#include "preferencewidget.h"
#include "programconfig.h"

/************
  * MenuBar类，主窗口的菜单栏
  ***********/
class MenuBar : public QMenuBar
{
    Q_OBJECT
public:
    explicit MenuBar(ProjectTabWidget *ptw, // 工程管理区指针
                     EditorTabWidget *etw, // 代码编辑区指针
                     const QList<QDockWidget *> &docks, // 主界面各个停靠窗口的指针
                     ProgramConfig *configFile, // 配置文件
                     QWidget *parent = nullptr); // 父窗口为主窗口

public slots:
    void on_preferenceButtonClicked();

private:
    QMenu* menuFile;
    QMenu* menuEdit;
    QMenu* menuView;
    QMenu* menuViewPanel;
    QMenu* menuViewZoom;
    QMenu* menuProject;
    QMenu* menuSource;
    QMenu* menuProcess;
    QMenu* menuTools;

    ProgramConfig *m_configFile;
};

#endif // MENUBAR_H
