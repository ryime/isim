/********************************************************************************
  * @file    newprojectwizard.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for NewProjectWizard.
  ******************************************************************************/

#ifndef NEWPROJECTWIZARD_H
#define NEWPROJECTWIZARD_H

#include <QWizard>
#include <QMessageBox>
#include <QWizardPage>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QDir>
#include <QLabel>
#include <QFileDialog>
#include <QMessageBox>
#include <QListView>
#include <QStandardItemModel>
#include "nomarginlayout.h"
#include "showdirlineedit.h"
#include "showdirlistview.h"
#include "projectitem.h"
#include "programconfig.h"

class NewProjectPage1;
class NewProjectPage2;

/************
  * NewProjectWizard类，是一个新建工程的向导，向导包含两页
  ***********/
class NewProjectWizard : public QWizard
{
    Q_OBJECT
public:
    explicit NewProjectWizard(ProgramConfig *const config, QWidget *parent = nullptr);

    // 当向导完成时，调用该函数
    void accept() override;

signals:
    // 当向导完成时，发射该信号
    void wizardFinished(const QString& projectFilePath);

private:
    NewProjectPage1* page1;
    NewProjectPage2* page2;
};

/************
  * NewProjectPage1类，是向导的第一页，设置工程的名字、路径和注释。
  ***********/
class NewProjectPage1 : public QWizardPage
{
    Q_OBJECT
public:
    explicit NewProjectPage1(ProgramConfig *const config, QWidget *parent = nullptr);

    // 检测工程文件夹创建是否成功
    bool validatePage() override;

    // 检测是否完成名字与路径的输入
    bool isComplete() const override;

    // 返回工程名字
    QString projectName() const;

    // 返回工程路径
    QString projectPath() const;

    // 返回工程注释
    QString projectComment() const;

private:
    QString upPath; // 工程路径的上一级目录
    QLineEdit* projectNameEdit;
    ShowDirLineEdit* projectPathEdit;
    QTextEdit* projectCommentEdit;
    ProgramConfig *const m_config;
};

/************
  * NewProjectPage2类，是向导的第二页，向工程内添加源文件。
  ***********/
class NewProjectPage2 : public QWizardPage
{
    Q_OBJECT
public:
    explicit NewProjectPage2(ProgramConfig *const config, NewProjectPage1 *page1, QWidget *parent = nullptr);

    // 将源文件复制到工程路径下
    QStringList copySources();

private:
    NewProjectPage1 *page1;
    ShowDirListView* fileListView;
    ProgramConfig *const m_config;
};

#endif // NEWPROJECTWIZARD_H
