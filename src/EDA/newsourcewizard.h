/********************************************************************************
  * @file    newsourcewizard.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for NewSourceWizard.
  ******************************************************************************/

#ifndef NEWSOURCEWIZARD_H
#define NEWSOURCEWIZARD_H

#include <QWizard>
#include <QWizardPage>
#include <QMessageBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QTableView>
#include "nomarginlayout.h"
#include "project.h"
#include "showdirlineedit.h"
#include "comboboxitemdelegate.h"

class NewSourcePage1;
class NewSourcePage2;

/************
  * NewProjectWizard类，是一个在工程内新建源文件的向导
  * 第一页对源文件的文件名、路径进行设置，第二页对模块的时间刻度、参数、端口进行设置，
  * 最后根据代码模板生成源文件
  ***********/
class NewSourceWizard : public QWizard
{
    Q_OBJECT
public:
    explicit NewSourceWizard(Project *project, QWidget *parent = nullptr);

    void accept() override;

signals:
    // 当向导完成时，发射该信号
    void wizardFinished(const QString &sourcePath);

private:
    NewSourcePage1* page1;
    NewSourcePage2* page2;
};

/************
  * NewSourcePage1类，是源文件向导的第一页
  ***********/
class NewSourcePage1 : public QWizardPage
{
    Q_OBJECT
public:
    explicit NewSourcePage1(Project *project, QWidget *parent = nullptr);

    // 初始化该页
    void initializePage() override;

    // 检查是否完成名字与路径的输入
    bool isComplete() const override;

    // 获取源文件名字
    QString sourceName() const;

    // 获取模块名字
    QString moduleName() const;

    // 获取源文件路径
    QString sourcePath() const;

private:
    QString upPath; // 源文件上一级目录的路径
    QLineEdit *sourceNameEdit;
    QComboBox *sourceTypeCombo;
    QLineEdit *moduleNameEdit;
    ShowDirLineEdit *sourcePathEdit;
    Project *m_project;
};

/************
  * NewSourcePage2类，是源文件向导的第二页，提供代码模板生成功能
  ***********/
class NewSourcePage2 : public QWizardPage
{
    Q_OBJECT
public:
    explicit NewSourcePage2(QWidget *parent = nullptr);

    // 检验输入是否符合要求
    bool validatePage() override;

    // 时间单位
    QString timeunit() const;

    // 时间精度
    QString timeprecision() const;

    // 参数列表
    QString parameterList() const;

    // 端口列表
    QString portList() const;

    // 端口声明的字符串
    QString portDeclarationList() const;

private:
    QComboBox *timeUnitCombo;
    QComboBox *timePrecisionCombo;

    QTableView *parameterTable;
    QStandardItemModel *parameterModel;
    QPushButton *addParameterButton;
    QPushButton *removeParameterButton;

    QTableView *portTable;
    QStandardItemModel *portModel;    
    QPushButton *addPortButton;
    QPushButton *removePortButton;
};

#endif // NEWSOURCEWIZARD_H
