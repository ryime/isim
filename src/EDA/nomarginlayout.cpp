/********************************************************************************
  * @file    nomarginlayout.cpp
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file provides all NoMarginHBoxLayout,
  *          NoMarginVBoxLayout and NoMarginGridLayout functions.
  *******************************************************************************/

#include "nomarginlayout.h"

NoMarginHBoxLayout::NoMarginHBoxLayout(QWidget *parent)
    : QHBoxLayout(parent)
{
    setContentsMargins(0, 0, 0, 0);
}

NoMarginVBoxLayout::NoMarginVBoxLayout(QWidget *parent)
    : QVBoxLayout(parent)
{
    setContentsMargins(0, 0, 0, 0);
}

NoMarginGridLayout::NoMarginGridLayout(QWidget *parent)
    : QGridLayout(parent)
{
    setContentsMargins(0, 0, 0, 0);
}
