/********************************************************************************
  * @file    nomarginlayout.h
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file contains all the functions prototypes for
  *          NoMarginHBoxLayout, NoMarginVBoxLayout and NoMarginGridLayout.
  ******************************************************************************/

#ifndef NOMARGINLAYOUT_H
#define NOMARGINLAYOUT_H

#include <QLayout>

/************
  * NoMarginHBoxLayout类，是一个横向的布局，将外边距设为0。
  ***********/
class NoMarginHBoxLayout : public QHBoxLayout
{
    Q_OBJECT
public:
    explicit NoMarginHBoxLayout(QWidget *parent = nullptr);
};

/************
  * NoMarginVBoxLayout类，是一个纵向的布局，将外边距设为0。
  ***********/
class NoMarginVBoxLayout : public QVBoxLayout
{
    Q_OBJECT
public:
    explicit NoMarginVBoxLayout(QWidget *parent = nullptr);
};

/************
  * NoMarginGridLayout类，是一个网格布局，将外边距设为0。
  ***********/
class NoMarginGridLayout : public QGridLayout
{
    Q_OBJECT
public:
    explicit NoMarginGridLayout(QWidget *parent = nullptr);
};

#endif // NOMARGINLAYOUT_H
