/********************************************************************************
  * @file    preferencewidget.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for PreferenceWidget.
  ******************************************************************************/

#ifndef PREFERENCEWIDGET_H
#define PREFERENCEWIDGET_H

#include <QWidget>
#include <QListWidget>
#include <QListWidgetItem>
#include <QStackedWidget>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QFontDialog>
#include <QSpinBox>
#include <QFileDialog>
#include <QListWidget>
#include <QSpacerItem>
#include <QCheckBox>
#include <QScrollArea>
#include <QTableView>
#include <QStandardItemModel>
#include <QComboBox>
#include "nomarginlayout.h"
#include "showcolorlineedit.h"
#include "showdirlineedit.h"
#include "showdirlistview.h"
#include "programconfig.h"

/************
  * PreferenceWidget类，是一个首选项设置窗口。窗口包含三个部分：
  * iverilog的设置，代码编辑器的设置，语言设置（保留）。
  ***********/
class PreferenceWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PreferenceWidget(ProgramConfig *const config, QWidget *parent = nullptr);

private:
    QListWidget *categoriesList;
    QLabel *categoriesLabel;
    QStackedWidget *stackedWidget;
    QDialogButtonBox *buttonBox;

    QWidget* toolConfigWidget;
    void initializeToolConfig();
    QWidget* fontConfigWidget;
    void initializeFontConfig();
    QWidget* languageConfigWidget;
    void initializeLanguageConfig();

    ProgramConfig *const m_config;

    ShowDirLineEdit *compileToolPathEdit;
    ShowDirLineEdit *waveToolPathEdit;
    QCheckBox *logEnableBox;
    QCheckBox *loadHierarchyEnableBox;
    ShowDirLineEdit *ivlcmdFilePathEdit;
    QList<QCheckBox *>ivlDebugFlagBoxes;
    QList<QCheckBox *>ivlGenerationFlagBoxes;
    QCheckBox *ivlIgnoreBox;
    ShowDirListView *ivlVPIModulePathList;
    ShowDirListView *ivlLibraryFileList;
    ShowDirListView *ivllibraryPathList;
    ShowDirLineEdit *ivlOutputFileEdit;
    QTableView *macroTable;
    QStandardItemModel *macroModel;
    QLineEdit *vvpExtraArg;

    QFontDialog *fontDialog;
    QSpinBox *tabWidthBox;
    ShowColorLineEdit *keywordColorEdit;
    ShowColorLineEdit *quotationColorEdit;
    ShowColorLineEdit *numberColorEdit;
    ShowColorLineEdit *commentColorEdit;
    ShowColorLineEdit *systemFunctionColorEdit;

    QComboBox *languageBox;

    bool m_propertyChanged;
    void storeSetting();

    QFrame *createHorizontalSeparator();

private slots:
    // 当设置的任一属性变更时，执行该函数
    void on_propertyChanged();
};

#endif // PREFERENCEWIDGET_H
