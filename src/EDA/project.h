/********************************************************************************
  * @file    project.h
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file contains all the functions prototypes for Project.
  ******************************************************************************/

#ifndef PROJECT_H
#define PROJECT_H

#include <QStandardItemModel>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QFileInfo>
#include <QMessageBox>
#include <QProcess>
#include <QSaveFile>
#include <QSettings>
#include <QDir>
#include <QMetaType>
#include "projectitem.h"
#include "hierarchymodel.h"

/************
  * Project类，代表了一个工程。
  ***********/
class Project : public QStandardItemModel
{
    Q_OBJECT
public:
    explicit Project(const QString &projectName, const QString &projectPath,
                    QSettings* projectFile, QObject *parent = nullptr);

    // 向工程内添加源文件
    void addSource(Source *newFile);

    // 移除工程内的源文件
    void removeSource(Source *file);

    // 保存工程
    void saveProject();

    // 加载工程
    void loadProject();

    // 代表工程名的项
    const ProjectItem *topItem() const;

    // 工程内保存源文件的路径
    QString sourcePath() const;

    // 工程内用于编译的路径
    QString compilePath() const;

    // 获取工程名字
    const QString &projectName() const;
    void setProjectName(const QString &newProjectName);

    // 获取工程路径
    const QString &projectPath() const;
    void setProjectPath(const QString &newProjectPath);

    // 获取工程注释
    const QString &projectComment() const;
    void setProjectComment(const QString &newProjectComment);

    // 根据index获取工程项
    ProjectItem *itemFromIndex(const QModelIndex &index) const;

    // 工程内源文件的列表
    const QList<Source *> &fileList() const;

private:
    QString m_projectName;
    QString m_projectPath;
    QString m_projectComment;
    QSettings* projectSaveFile;
    ProjectItem* m_topItem;
    QList<Source *> m_fileList;
};

#endif // PROJECT_H
