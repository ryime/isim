/********************************************************************************
  * @file    projectfiletab.cpp
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file provides all the ProjectFileTab functions.
  ******************************************************************************/

#include "projectfiletab.h"

#include <QHeaderView>

ProjectFileTab::ProjectFileTab(QWidget *parent)
    : QTreeView(parent)
{
    setEditTriggers(QTreeView::NoEditTriggers);
    setContextMenuPolicy(Qt::CustomContextMenu);
    setExpandsOnDoubleClick(false);
    setHeaderHidden(true);
    setHorizontalScrollMode(QTreeView::ScrollPerPixel);
    header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    header()->setStretchLastSection(true);
}
