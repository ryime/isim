/********************************************************************************
  * @file    projectfiletab.h
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file contains all the functions prototypes for ProjectFileTab.
  ******************************************************************************/

#ifndef PROJECTFILETAB_H
#define PROJECTFILETAB_H

#include <QTreeView>

/************
  * ProjectFileTab类，用于显示工程内的文件列表。
  ***********/
class ProjectFileTab : public QTreeView
{
    Q_OBJECT
public:
    explicit ProjectFileTab(QWidget *parent = nullptr);
};

#endif // PROJECTFILETAB_H
