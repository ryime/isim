/********************************************************************************
  * @file    projectitem.cpp
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file provides all the ProjectItem functions.
  ******************************************************************************/

#include "projectitem.h"

ProjectItem::ProjectItem(const QString &name, TreeItemType type, Source *file)
    : QStandardItem(name),
      m_file(file)
{
    setItemType(type);
}

ProjectItem::TreeItemType ProjectItem::itemType() const
{
    return m_itemType;
}

void ProjectItem::setItemType(TreeItemType newType)
{
    m_itemType = newType;
    switch (newType) {
    case NoCompiled:
        setIcon(QIcon(":/icon/Question.png"));
        break;
    case ErrorCompiled:
        setIcon(QIcon(":/icon/Error.png"));
        removeRows(0, rowCount());
        break;
    case Compiled:
        setCompiled();
        setIcon(QIcon(":/icon/Checked.png"));
        break;
    case ErrorRun:
        setIcon(QIcon(":/icon/Error.png"));
        removeRows(1, rowCount() - 1);
        break;
    case Run:
        setRun();
        setIcon(QIcon(":/icon/Checked.png"));
        break;
    case Intermediate:
        setIcon(QIcon(":/icon/CorFile.png"));
        break;
    case WaveFile:
        setIcon(QIcon(":/icon/WaveFile.png"));
        break;
    default:
        setIcon(QIcon());
        break;
    }
}

Source *ProjectItem::file()
{
    return m_file;
}

const QList<HierarchyModel *> &ProjectItem::hierModels() const
{
    return m_hierModels;
}

void ProjectItem::setHierModels(const QList<HierarchyModel *> &newHierModels)
{
    m_hierModels = newHierModels;
}

bool ProjectItem::isWaveOpening() const
{
    return m_isWaveOpening;
}

void ProjectItem::setIsWaveOpening(bool newWaveOpening)
{
    m_isWaveOpening = newWaveOpening;
}

ProjectItem *ProjectItem::child(int row, int column) const
{
    return dynamic_cast<ProjectItem *>(QStandardItem::child(row,column));
}

void ProjectItem::setCompiled()
{
    if (rowCount() == 0) {
        appendRow(new ProjectItem(QFileInfo(m_file->compiledFilePath()).fileName(), Intermediate, m_file));
    } else {
        if (child(0)->itemType() == Intermediate) {
            child(0)->setText(QFileInfo(m_file->compiledFilePath()).fileName());
        } else {
            insertRow(0, new ProjectItem(QFileInfo(m_file->compiledFilePath()).fileName(), Intermediate, m_file));
        }
    }
}

void ProjectItem::setRun()
{
    setCompiled();
    if (!m_file->waveFilePath().isEmpty()) {
        QStringList waveList = m_file->waveFilePath().split(' ');
        int waveCount = waveList.length();
        if (rowCount() - 1 < waveCount) {
            for (int i = rowCount() - 1; i < waveCount; ++i) {
                appendRow(new ProjectItem(QString(), WaveFile, m_file));
            }
        } else {
            removeRows(waveCount + 1, rowCount() - 1 - waveCount);
        }
        for (int i = 1; i < waveCount + 1; ++i) {
            child(i)->setText(QFileInfo(waveList.at(i - 1)).fileName());
        }
    }
}
