/********************************************************************************
  * @file    projectitem.h
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file contains all the functions prototypes for ProjectItem.
  ******************************************************************************/

#ifndef PROJECTITEM_H
#define PROJECTITEM_H

#include <QStandardItem>
#include <QObject>
#include "hierarchymodel.h"
#include "source.h"

/************
  * ProjectItem类，是工程管理区文件树的项。
  ***********/
class ProjectItem : public QStandardItem
{
public:
    // 该项的类型
    enum TreeItemType {
        Project, // 工程
        NoCompiled, ErrorCompiled, Compiled, ErrorRun, Run, // 源文件（包含其编译状态）
        Intermediate, // 中间文件
        WaveFile // 波形文件
    };

    explicit ProjectItem(const QString &name,
                         TreeItemType type,
                         Source *file = nullptr);

    // 获得该项的类型
    TreeItemType itemType() const;
    void setItemType(TreeItemType newitemType);

    // 获得该项对应的文件
    Source *file();
    void setFile(Source *newFile);

    // 获得该项对应的Verilog层次结构
    const QList<HierarchyModel *> &hierModels() const;
    void setHierModels(const QList<HierarchyModel *> &newHierModels);

    // 获得该项对应的波形文件是否已经打开
    bool isWaveOpening() const;
    void setIsWaveOpening(bool newWaveOpening);

    ProjectItem *child(int row, int column = 0) const;

private:
    TreeItemType m_itemType;
    Source *m_file;
    QList<HierarchyModel *> m_hierModels;
    bool m_isWaveOpening = false;

    void setCompiled();
    void setRun();
};

#endif // PROJECTITEM_H
