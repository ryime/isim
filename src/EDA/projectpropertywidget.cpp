/********************************************************************************
  * @file    projectpropertywidget.cpp
  * @author  Lun Li
  * @version V2.2.1
  * @date    2022.9.4
  * @brief   This file provides all ProjectPropertyWidget functions.
  ******************************************************************************/

#include "projectpropertywidget.h"
#include "showdirlistview.h"

#include <QLabel>
#include <QListWidget>
#include <QScrollArea>
#include <QStackedWidget>
#include <QTextEdit>
#include "nomarginlayout.h"
#include "showdirlineedit.h"

ProjectPropertyWidget::ProjectPropertyWidget(Project *project, QWidget *parent)
    : QWidget{parent}
{
    QLabel *projectNameLabel = new QLabel(tr("Project Name:"), this);
    QLineEdit *projectName = new QLineEdit(project->projectName(), this);
    projectName->setReadOnly(true);
    QLabel *projectPathLabel = new QLabel(tr("Project Path:"), this);
    ShowDirLineEdit *projectPath = new ShowDirLineEdit(this);
    projectPath->setText(project->projectPath());
    projectPath->lineEdit()->setReadOnly(true);
    QLabel *projectCommentLabel = new QLabel(tr("Project Comment:"), this);
    QTextEdit *projectComment = new QTextEdit(project->projectComment(), this);
    projectComment->setReadOnly(true);

    QScrollArea *propertyArea = new QScrollArea(this);
    QWidget *propertyWidget = new QWidget(this);
    propertyWidget->setLayout(new NoMarginVBoxLayout);
    propertyWidget->layout()->addWidget(projectNameLabel);
    propertyWidget->layout()->addWidget(projectName);
    propertyWidget->layout()->addWidget(projectPathLabel);
    propertyWidget->layout()->addWidget(projectPath);
    propertyWidget->layout()->addWidget(projectCommentLabel);
    propertyWidget->layout()->addWidget(projectComment);
    propertyArea->setWidget(propertyWidget);

    ShowDirListView *fileListView = new ShowDirListView(this);
    QStringList fileList;
    for (Source *source : project->fileList()) {
        fileList << source->fileName();
    }
    fileListView->addDirs(fileList);

    QStackedWidget *stackedWidget = new QStackedWidget;
    stackedWidget->addWidget(propertyArea);
    stackedWidget->addWidget(fileListView);
    QListWidget *categoriesList = new QListWidget;
    categoriesList->addItems(QStringList() << tr("Properties") << tr("Sources"));
    categoriesList->setEditTriggers(QListWidget::NoEditTriggers);

    NoMarginHBoxLayout *hLayout = new NoMarginHBoxLayout;
    setLayout(hLayout);
    hLayout->addWidget(categoriesList);
    hLayout->addWidget(stackedWidget);
    hLayout->setStretch(0, 1);
    hLayout->setStretch(1, 5);

    connect(categoriesList, &QListWidget::currentRowChanged, stackedWidget, &QStackedWidget::setCurrentIndex);

    categoriesList->setCurrentRow(0);
    setWindowTitle(tr("Project Property"));
    setAttribute(Qt::WA_DeleteOnClose);
}
