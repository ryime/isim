/********************************************************************************
  * @file    projectpropertywidget.h
  * @author  Lun Li
  * @version V2.2.1
  * @date    2022.9.4
  * @brief   This file contains all the functions prototypes for ProjectPropertyWidget.
  ******************************************************************************/

#ifndef PROJECTPROPERTYWIDGET_H
#define PROJECTPROPERTYWIDGET_H

#include <QWidget>
#include "project.h"

/************
  * ProjectPropertyWidget类，是工程的属性窗口。
  ***********/
class ProjectPropertyWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ProjectPropertyWidget(Project *project, QWidget *parent = nullptr);
};

#endif // PROJECTPROPERTYWIDGET_H
