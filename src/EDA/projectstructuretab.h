/********************************************************************************
  * @file    projectstructuretab.h
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file contains all the functions prototypes for ProjectStructureTab.
  ******************************************************************************/

#ifndef PROJECTSTRUCTURETAB_H
#define PROJECTSTRUCTURETAB_H

#include <QComboBox>
#include <QLineEdit>
#include <QListView>
#include <QMenu>
#include <QTreeView>
#include <QWidget>
#include "hierarchymodel.h"
#include "nomarginlayout.h"
#include "project.h"

/************
  * ProjectStructureTab类，用于展示工程每一项对应的Verilog层次结构及其内部的变量。
  ***********/
class ProjectStructureTab : public QWidget
{
    Q_OBJECT
public:
    explicit ProjectStructureTab(QWidget *parent = nullptr);

public slots:
    // 添加层次结构
    void addModel(HierarchyModel *model);

    // 移除层次结构
    void removeModel(HierarchyModel *model);

signals:
    // 当层次结构的某一项被点击时，发出该信号，表示需要打开文件
    void hierarchyItemDoubleClicked(Source *file);

    // 当某个变量需要添加到波形显示窗口时，发出该信号
    void addingVariableRequested(const QString &name);

private:
    enum VariableAction {
        Copy, CopyHierarchy, AddVariableToWaveform
    };
    QComboBox *topLevelName;
    QTreeView *hierarchyView;
    QLineEdit *searchVariable;
    QListView *variableListView;

    QList<HierarchyModel *> modelList;
    HierarchyItem *curHierarchyItem;
    HierarchyModel *curHierarchyModel;
    QStandardItemModel *filterVariableModel;
    QStandardItemModel *variableModel;
    QMenu *variableMenu;
    QList<QAction *>actionList;

    void initializeGUI();

private slots:
    void on_topLevelChanged(int index);
    void on_searchStringChanged(const QString &text);
    void on_hierarchyViewClicked(const QModelIndex &index);
    void on_hierarchyViewDoubleClicked(const QModelIndex &index);
    void on_variableMenuRequested(const QPoint &pos);
    void on_copyVariableNameClicked();
    void on_copyvariableHierarchyClicked();
    void on_addVariableToWaveformClicked();

};

#endif // PROJECTSTRUCTURETAB_H
