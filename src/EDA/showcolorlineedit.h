/********************************************************************************
  * @file    showcolorlineedit.h
  * @author  Lun Li
  * @version 1.5.0
  * @date    2022.4.10
  * @brief   This file contains all the functions prototypes for the ShowColorLineEdit.
  ******************************************************************************/

#ifndef SHOWCOLORLINEEDIT_H
#define SHOWCOLORLINEEDIT_H

#include <QLineEdit>

/************
  * ShowColorLineEdit类，是一个用于显示颜色并支持用户修改颜色的LineEdit
  ***********/
class ShowColorLineEdit : public QWidget
{
    Q_OBJECT
public:
    // 带label
    explicit ShowColorLineEdit(const QString &labelText,
                               const QColor &color,
                               const QString &text,
                               QWidget *parent = nullptr);

    // 不带label
    explicit ShowColorLineEdit(const QColor &color,
                               const QString &text,
                               QWidget *parent = nullptr);

    const QColor &color() const;

signals:
    void colorChanged();

private:
    QColor m_color;
    void initialize(const QColor &color);
    QLineEdit *m_lineEdit;
};

#endif // SHOWCOLORLINEEDIT_H
