/********************************************************************************
  * @file    showdirlineedit.h
  * @author  Lun Li
  * @version 1.5.0
  * @date    2022.4.10
  * @brief   This file contains all the functions prototypes for the ShowDirLineEdit.
  ******************************************************************************/

#ifndef SHOWDIRLINEEDIT_H
#define SHOWDIRLINEEDIT_H

#include <QLineEdit>
#include <QWidget>
#include <QDir>
#include <QLabel>
#include <QPushButton>
#include "nomarginlayout.h"

/************
  * ShowDirLineEdit类，是一个用于显示文件路径的LineEdit，可提供一个浏览按钮
  * 该LineEdit会将路径的分隔符自适应显示，linux为/，windows为\
  ***********/
class ShowDirLineEdit : public QWidget
{
    Q_OBJECT
public:
    // 带label和button
    explicit ShowDirLineEdit(const QString &labelText,
                             const QString &buttonText,
                             QWidget *parent = nullptr);

    // 带label
    explicit ShowDirLineEdit(QLabel *label, QWidget *parent = nullptr);

    // 带button
    explicit ShowDirLineEdit(const QString &buttonText, QWidget *parent = nullptr);

    explicit ShowDirLineEdit(QWidget *parent = nullptr);

    // 文本
    void setText(const QString &text);
    QString text() const;

    QPushButton *button() const;
    QLineEdit *lineEdit() const;

signals:
    void textChanged(const QString &text);
    void textEdited(const QString &text);

private:
    QString m_text;
    QLineEdit *m_lineEdit;
    QPushButton *m_button;
    void connectSignals();
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
};

#endif // SHOWDIRLINEEDIT_H
