/********************************************************************************
  * @file    showdirlistview.cpp
  * @author  Lun Li
  * @version V1.5.0
  * @date    2022.4.10
  * @brief   This file provides all the ShowDirListView functions.
  ******************************************************************************/

#include "showdirlistview.h"

ShowDirListView::ShowDirListView(const QString &labelText, const QString &buttonAddText, const QString &buttonRemoveText, QWidget *parent)
    : QWidget(parent),
      m_model(new QStringListModel(this)),
      m_listView(new QListView(this)),
      m_addButton(new QPushButton(buttonAddText, this)),
      m_removeButton(new QPushButton(buttonRemoveText, this))
{
    setLayout(new NoMarginVBoxLayout);
    QWidget *buttonWidget = new QWidget(this);
    buttonWidget->setLayout(new NoMarginVBoxLayout);
    buttonWidget->layout()->addWidget(m_addButton);
    buttonWidget->layout()->addWidget(m_removeButton);
    QWidget *hWidget = new QWidget(this);
    hWidget->setLayout(new NoMarginHBoxLayout);
    hWidget->layout()->addWidget(m_listView);
    hWidget->layout()->addWidget(buttonWidget);
    layout()->addWidget(new QLabel(labelText));
    layout()->addWidget(hWidget);
    m_listView->setEditTriggers(QListView::NoEditTriggers);
    m_listView->setSelectionMode(QListView::ExtendedSelection);
    m_listView->setModel(m_model);
    m_removeButton->setEnabled(false);
}

ShowDirListView::ShowDirListView(const QString &buttonAddText, const QString &buttonRemoveText, QWidget *parent)
    : QWidget(parent),
      m_model(new QStringListModel(this)),
      m_listView(new QListView(this)),
      m_addButton(new QPushButton(buttonAddText, this)),
      m_removeButton(new QPushButton(buttonRemoveText, this))
{
    setLayout(new NoMarginHBoxLayout);
    QWidget *buttonWidget = new QWidget(this);
    buttonWidget->setLayout(new NoMarginVBoxLayout);
    buttonWidget->layout()->addWidget(m_addButton);
    buttonWidget->layout()->addWidget(m_removeButton);
    layout()->addWidget(m_listView);
    layout()->addWidget(buttonWidget);
    m_listView->setEditTriggers(QListView::NoEditTriggers);
    m_listView->setSelectionMode(QListView::ExtendedSelection);
    m_listView->setModel(m_model);
    m_removeButton->setEnabled(false);
}

ShowDirListView::ShowDirListView(QWidget *parent)
    : QWidget(parent),
      m_model(new QStringListModel(this)),
      m_listView(new QListView(this)),
      m_addButton(nullptr),
      m_removeButton(nullptr)
{
    setLayout(new NoMarginHBoxLayout);
    layout()->addWidget(m_listView);
    m_listView->setEditTriggers(QListView::NoEditTriggers);
    m_listView->setSelectionMode(QListView::ExtendedSelection);
    m_listView->setModel(m_model);
}

void ShowDirListView::addDirs(const QStringList &dirs)
{
    QStringList originalDirs = m_model->stringList();
    for (const QString &dir : dirs) {
        originalDirs << QDir::toNativeSeparators(dir);
    }
    m_model->setStringList(originalDirs);
    if (!dirs.isEmpty() && m_removeButton) {
        m_removeButton->setEnabled(true);
    }
    emit listChanged();
}

void ShowDirListView::removeSelectedDirs()
{
    QModelIndexList indexes = m_listView->selectionModel()->selectedIndexes();
    while (!indexes.isEmpty()) {
        m_model->removeRow(indexes.at(0).row());
        indexes = m_listView->selectionModel()->selectedIndexes();
    }
    if (m_model->rowCount() == 0 && m_removeButton) {
        m_removeButton->setEnabled(false);
    }
    emit listChanged();
}

QStringList ShowDirListView::dirs() const
{
    QStringList rtn;
    for (const QString &path : m_model->stringList()) {
        rtn << QDir::fromNativeSeparators(path);
    }
    return rtn;
}

QPushButton *ShowDirListView::addButton() const
{
    return m_addButton;
}

QPushButton *ShowDirListView::removeButton() const
{
    return m_removeButton;
}
