/********************************************************************************
  * @file    showdirlistview.h
  * @author  Lun Li
  * @version 1.5.0
  * @date    2022.4.10
  * @brief   This file contains all the functions prototypes for the ShowDirListView.
  ******************************************************************************/

#ifndef SHOWDIRLISTVIEW_H
#define SHOWDIRLISTVIEW_H

#include <QLabel>
#include <QListView>
#include <QPushButton>
#include <QDir>
#include <QStringListModel>
#include "nomarginlayout.h"

/************
  * ShowDirLineEdit类，是一个用于显示多个文件路径，支持添加和删除的ListView
  * 该ListView会将路径的分隔符自适应显示，linux为/，windows为\
  ***********/
class ShowDirListView : public QWidget
{
    Q_OBJECT
public:
    // 带label、添加删除按钮
    explicit ShowDirListView(const QString &labelText,
                             const QString &buttonAddText,
                             const QString &buttonRemoveText,
                             QWidget *parent = nullptr);

    // 不带label，带添加删除按钮
    explicit ShowDirListView(const QString &buttonAddText,
                             const QString &buttonRemoveText,
                             QWidget *parent = nullptr);

    // 不带label和按钮
    explicit ShowDirListView(QWidget *parent = nullptr);

    // 添加路径
    void addDirs(const QStringList &dirs);

    // 移除选中路径
    void removeSelectedDirs();

    // 获取路径
    QStringList dirs() const;

    QPushButton *addButton() const;
    QPushButton *removeButton() const;

signals:
    void listChanged();

private:
    QStringListModel *m_model;
    QListView *m_listView;
    QPushButton *m_addButton;
    QPushButton *m_removeButton;
};

#endif // SHOWDIRLISTVIEW_H
