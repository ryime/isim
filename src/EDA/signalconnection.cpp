/********************************************************************************
  * @file    signalconnection.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all the SignalConnection functions.
  ******************************************************************************/

#include "signalconnection.h"

SignalConnection globalSignal;

SignalConnection::SignalConnection(QObject *parent)
    : QObject{parent}
{
}
