/********************************************************************************
  * @file    signalconnection.h
  * @author  Lun Li
  * @version 2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for the SignalConnection.
  ******************************************************************************/

#ifndef SIGNALCONNECTION_H
#define SIGNALCONNECTION_H

#include <QObject>
#include <QDockWidget>
#include "source.h"
#include "hierarchymodel.h"

class Source;
class Project;
class HierarchyModel;

/************
  * SignalConnection类，用于连接各类的信号与槽
  ***********/
class SignalConnection : public QObject
{
    Q_OBJECT
public:
    explicit SignalConnection(QObject *parent = nullptr);

signals:
    // ProjectTabWidget
    void compileFilesRequested(const QList<Source *>&files, Project *project, bool merge);

    // ProjectTabWidget
    void runFileRequested(Source *file, Project *project);

    // ProjectTabWidget
    void compileCommandRequested(const QString &command);

    // ProjectTabWidget
    void showWaveRequested(const QString &command);

    // ProjectTabWidget
    void addWaveCommandRequested(const QString &command);

    // ProjectTabWidget
    void fileNeedOpened(Source *file);

    // ProjectTabWidget
    void fileNeedClosed(Source *file);

    // ProjectTabWidget
    void fileNeedSaved(Source *file);

    // ProjectTabWidget
    void projectOpened();

    // ProjectTabWidget
    void projectClosed();

    // ProjectTabWidget
    void showErrorLineRequested(Source *file, int lineNumber);

    // CompileProcess
    void compileStarted();

    // CompileProcess
    void compileInput(const QString &input);

    // CompileProcess
    void compileOutput(const QString &output);

    // CompileProcess
    void compileHint(const QString &hint);

    // CompileProcess
    void compileError(const QString &error);

    // CompileProcess
    void compileFinished(const QList<HierarchyModel *> &hierModels);

    // CompileProcess
    void VVPStarted();

    // CompileProcess
    void VVPFinished();

    // WaveProcess
    void waveInput(const QString &input);

    // WaveProcess
    void waveOutput(const QString &output);

    // WaveProcess
    void waveError(const QString &error);

    // ConsoleTabWidget
    void fileLineNeedTurned(const QString &filePath, int lineNumber);

    // ConsoleTabWidget
    void compileCmdIsInput(const QString &cmd);

    // ConsoleTabWidget
    void waveCmdIsInput(const QString &cmd);

    // ProgramConfig
    void editorConfigChanged();

    // EditorTabWidget
    void cursorPositionChanged(int line, int column);

    // ConsoleErrorTab
    void errorItemClicked(const QString &filePath, int lineNumber);

};



#endif // SIGNALCONNECTION_H
