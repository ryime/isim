/********************************************************************************
  * @file    source.cpp
  * @author  Lun Li
  * @version V1.4.0
  * @date    2022.3.23
  * @brief   This file provides all the Source functions.
  *******************************************************************************/

#include "source.h"

//constructor
Source::Source(const QString &path, bool isNewFile, QObject *parent)
    : QFile(path, parent),
      m_isNewFile(isNewFile),
      m_indexInEditor(-1),
      m_compiledFilePath(QString()),
      m_waveFilePath(QString()),
      m_projectItem(nullptr),
      m_indexInHierarchy(QPersistentModelIndex())
{

}

QString Source::fileName() const
{
    return QFileInfo(QFile::fileName()).fileName();
}

QString Source::filePath() const
{
    return QFile::fileName();
}

QString Source::fileType() const
{
    return QFileInfo(QFile::fileName()).completeSuffix();
}

QString Source::fileBaseName() const
{
    return QFileInfo(QFile::fileName()).completeBaseName();
}

int Source::indexInEditor() const
{
    return m_indexInEditor;
}

void Source::setIndexInEditor(int newIndexInEditor)
{
    m_indexInEditor = newIndexInEditor;
}

const QString &Source::compiledFilePath() const
{
    return m_compiledFilePath;
}

void Source::setCompiledFilePath(const QString &newCompiledFilePath)
{
    m_compiledFilePath = newCompiledFilePath;
}

const QString &Source::waveFilePath() const
{
    return m_waveFilePath;
}

void Source::setWaveFilePath(const QString &newWaveFilePath)
{
    m_waveFilePath = newWaveFilePath;
}

const QPersistentModelIndex &Source::indexInHierarchy() const
{
    return m_indexInHierarchy;
}

void Source::setIndexInHierarchy(const QPersistentModelIndex &newIndexInHierarchy)
{
    m_indexInHierarchy = newIndexInHierarchy;
}

bool Source::isNewFile() const
{
    return m_isNewFile;
}

void Source::setIsNewFile(bool newIsNewFile)
{
    m_isNewFile = newIsNewFile;
}

ProjectItem *Source::projectItem() const
{
    return m_projectItem;
}

void Source::setProjectItem(ProjectItem *newProjectItem)
{
    m_projectItem = newProjectItem;
}
