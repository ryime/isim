/********************************************************************************
  * @file    source.h
  * @author  Lun Li
  * @version V1.4.0
  * @date    2022.3.23
  * @brief   This file contains all the functions prototypes for Source.
  *******************************************************************************/

#ifndef SOURCE_H
#define SOURCE_H

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QPersistentModelIndex>

class ProjectItem;

/************
  * Source类，代表着一个源文件
  ***********/
class Source : public QFile
{
    Q_OBJECT

public:
    explicit Source(const QString &path, bool isNewFile = false, QObject *parent = nullptr);

    // 文件名
    QString fileName() const;

    // 文件路径
    QString filePath() const;

    // 文件类型
    QString fileType() const;

    // 文件名（不带后缀）
    QString fileBaseName() const;

    // 在编辑器中是第几个文件
    int indexInEditor() const;
    void setIndexInEditor(int newIndexInEditor);

    // 该文件对应的编译中间文件的路径
    const QString &compiledFilePath() const;
    void setCompiledFilePath(const QString &newCompiledFilePath);

    // 该文件对应的波形文件的路径
    const QString &waveFilePath() const;
    void setWaveFilePath(const QString &newWaveFilePath);

    // 该文件对应的在层次结构中的index
    const QPersistentModelIndex &indexInHierarchy() const;
    void setIndexInHierarchy(const QPersistentModelIndex &newIndexInHierarchy);

    // 该文件是否为新文件
    bool isNewFile() const;
    void setIsNewFile(bool newIsNewFile);

    // 该文件对应的在工程中的项
    ProjectItem *projectItem() const;
    void setProjectItem(ProjectItem *newProjectItem);

private:
    bool m_isNewFile;
    int m_indexInEditor;
    QString m_compiledFilePath;
    QString m_waveFilePath;
    ProjectItem *m_projectItem;
    QPersistentModelIndex m_indexInHierarchy;
};

#endif // SOURCE_H
