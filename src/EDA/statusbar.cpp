/********************************************************************************
  * @file    statusbar.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all StatusBar functions.
  ******************************************************************************/

#include "statusbar.h"
#include "signalconnection.h"

StatusBar::StatusBar(ProgramConfig *configFile, QWidget *parent)
    : QStatusBar(parent),
      editorDetailLabel(new QLabel(tr("Line:    Column:    TabWidth:    ")))
{
    addPermanentWidget(editorDetailLabel);

    extern SignalConnection globalSignal;
    connect(&globalSignal, &SignalConnection::cursorPositionChanged, this, [=](int line, int column) {
        setEditorDetail(line, column);
    });
    connect(&globalSignal, &SignalConnection::editorConfigChanged, this, [=]() {
        setEditorDetail(-1, -1, configFile->tabWidth());
    });
    connect(&globalSignal, &SignalConnection::compileStarted, this, [=] {
        showMessage(tr("Compiling..."));
    });
    connect(&globalSignal, &SignalConnection::compileFinished, this, [&]{
        clearMessage();
    });
    connect(&globalSignal, &SignalConnection::VVPStarted, this, [=] {
        showMessage(tr("Compiling..."));
    });
    connect(&globalSignal, &SignalConnection::VVPFinished, this, [=] {
        clearMessage();
    });
}

void StatusBar::setEditorDetail(int line, int column, int tabWidth)
{
    if (line >= 0) {
        editorDetailLine = line;
    }
    if (column >= 0) {
        editorDetailColumn = column;
    }
    if (tabWidth >= 0) {
        editorDetailTabWidth = tabWidth;
    }
    editorDetailLabel->setText(tr("Line:%1  Column:%2  TabWidth:%3  ")
                               .arg(editorDetailLine)
                               .arg(editorDetailColumn)
                               .arg(editorDetailTabWidth));
}
