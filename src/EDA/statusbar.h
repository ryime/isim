/********************************************************************************
  * @file    statusbarh
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for StatusBar.
  ******************************************************************************/

#ifndef STATUSBAR_H
#define STATUSBAR_H

#include "programconfig.h"
#include <QLabel>
#include <QStatusBar>

/************
  * StatusBar类，主窗口的状态栏
  ***********/
class StatusBar : public QStatusBar
{
    Q_OBJECT
public:
    explicit StatusBar(ProgramConfig *configFile,
                       QWidget *parent = nullptr);

public slots:
    void setEditorDetail(int line = -1, int column = -1, int tabWidth = -1);

private:
    QLabel *editorDetailLabel;
    int editorDetailLine;
    int editorDetailColumn;
    int editorDetailTabWidth;
};

#endif // STATUSBAR_H
