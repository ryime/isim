/********************************************************************************
  * @file    toolbar.cpp
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file provides all ToolBar functions.
  ******************************************************************************/

#include "toolbar.h"

ToolBar::ToolBar(ProjectTabWidget *ptw,
                 EditorTabWidget *etw,
                 QWidget *parent)
    : QToolBar(parent)
{
    setIconSize(QSize(15,15));
    addActions(QList<QAction*>()
                << etw->actionList().at(EditorTabWidget::NewFile)
                << etw->actionList().at(EditorTabWidget::OpenFile)
                << etw->actionList().at(EditorTabWidget::CloseFile)
                << etw->actionList().at(EditorTabWidget::SaveFile)
                << ptw->actionList().at(ProjectTabWidget::NewProject)
                << ptw->actionList().at(ProjectTabWidget::OpenProject)
                << etw->actionList().at(EditorTabWidget::Cut)
                << etw->actionList().at(EditorTabWidget::Copy)
                << etw->actionList().at(EditorTabWidget::Paste)
                << etw->actionList().at(EditorTabWidget::Undo)
                << etw->actionList().at(EditorTabWidget::Redo)
                << etw->actionList().at(EditorTabWidget::ZoomIn)
                << etw->actionList().at(EditorTabWidget::ZoomOut)
                << ptw->actionList().at(ProjectTabWidget::Compile)
                << ptw->actionList().at(ProjectTabWidget::Run));
}
