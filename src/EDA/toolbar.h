/********************************************************************************
  * @file    toolbar.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for ToolBar.
  ******************************************************************************/

#ifndef TOOLBAR_H
#define TOOLBAR_H

#include "projecttabwidget.h"
#include "editortabwidget.h"
#include <QToolBar>

/************
  * ToolBar类，主窗口的工具栏
  ***********/
class ToolBar : public QToolBar
{
    Q_OBJECT
public:
    explicit ToolBar(ProjectTabWidget *ptw,
                     EditorTabWidget *etw,
                     QWidget *parent = nullptr);
};

#endif // TOOLBAR_H
