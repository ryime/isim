/********************************************************************************
  * @file    verilogcompleter.cpp
  * @author  Lun Li
  * @version V2.2.1
  * @date    2022.9.5
  * @brief   This file provides all the VerilogCompleter functions.
  *******************************************************************************/

#include "verilogcompleter.h"

#include <QStringListModel>

VerilogCompleter::VerilogCompleter(QObject *parent)
    : QCompleter{parent}
{
    QStringList keywords_1800_2005 = {
        QStringLiteral("alias"),          QStringLiteral("endmodule"),      QStringLiteral("matches"),                QStringLiteral("small"),
        QStringLiteral("always"),         QStringLiteral("endpackage"),     QStringLiteral("medium"),                 QStringLiteral("solve"),
        QStringLiteral("always_comb"),    QStringLiteral("endprimitive"),   QStringLiteral("modport"),                QStringLiteral("specify"),
        QStringLiteral("always_ff"),      QStringLiteral("endprogram"),     QStringLiteral("module"),                 QStringLiteral("specparam"),
        QStringLiteral("always_latch"),   QStringLiteral("endproperty"),    QStringLiteral("nand"),                   QStringLiteral("static"),
        QStringLiteral("and"),            QStringLiteral("endspecify"),     QStringLiteral("negedge"),                QStringLiteral("string"),
        QStringLiteral("assert"),         QStringLiteral("endsequence"),    QStringLiteral("new"),                    QStringLiteral("strong0"),
        QStringLiteral("assign"),         QStringLiteral("endtable"),       QStringLiteral("nmos"),                   QStringLiteral("strong1"),
        QStringLiteral("assume"),         QStringLiteral("endtask"),        QStringLiteral("nor"),                    QStringLiteral("struct"),
        QStringLiteral("automatic"),      QStringLiteral("enum"),           QStringLiteral("noshowcancelled"),        QStringLiteral("super"),
        QStringLiteral("before"),         QStringLiteral("event"),          QStringLiteral("not"),                    QStringLiteral("supply0"),
        QStringLiteral("begin"),          QStringLiteral("expect"),         QStringLiteral("notif0"),                 QStringLiteral("supply1"),
        QStringLiteral("bind"),           QStringLiteral("export"),         QStringLiteral("notif1"),                 QStringLiteral("table"),
        QStringLiteral("bins"),           QStringLiteral("extends"),        QStringLiteral("null"),                   QStringLiteral("tagged"),
        QStringLiteral("binsof"),         QStringLiteral("extern"),         QStringLiteral("or"),                     QStringLiteral("task"),
        QStringLiteral("bit"),            QStringLiteral("final"),          QStringLiteral("output"),                 QStringLiteral("this"),
        QStringLiteral("break"),          QStringLiteral("first_match"),    QStringLiteral("package"),                QStringLiteral("throughout"),
        QStringLiteral("buf"),            QStringLiteral("for"),            QStringLiteral("packed"),                 QStringLiteral("time"),
        QStringLiteral("bufif0"),         QStringLiteral("force"),          QStringLiteral("parameter"),              QStringLiteral("timeprecision"),
        QStringLiteral("bufif1"),         QStringLiteral("foreach"),        QStringLiteral("pmos"),                   QStringLiteral("timeunit"),
        QStringLiteral("byte"),           QStringLiteral("forever"),        QStringLiteral("posedge"),                QStringLiteral("tran"),
        QStringLiteral("case"),           QStringLiteral("fork"),           QStringLiteral("primitive"),              QStringLiteral("tranif0"),
        QStringLiteral("casex"),          QStringLiteral("forkjoin"),       QStringLiteral("priority"),               QStringLiteral("tranif1"),
        QStringLiteral("casez"),          QStringLiteral("function"),       QStringLiteral("program"),                QStringLiteral("tri"),
        QStringLiteral("cell"),           QStringLiteral("generate"),       QStringLiteral("property"),               QStringLiteral("tri0"),
        QStringLiteral("chandle"),        QStringLiteral("genvar"),         QStringLiteral("protected"),              QStringLiteral("tri1"),
        QStringLiteral("class"),          QStringLiteral("highz0"),         QStringLiteral("pull0"),                  QStringLiteral("triand"),
        QStringLiteral("clocking"),       QStringLiteral("highz1"),         QStringLiteral("pull1"),                  QStringLiteral("trior"),
        QStringLiteral("cmos"),           QStringLiteral("if"),             QStringLiteral("pulldown"),               QStringLiteral("trireg"),
        QStringLiteral("config"),         QStringLiteral("iff"),            QStringLiteral("pullup"),                 QStringLiteral("type"),
        QStringLiteral("const"),          QStringLiteral("ifnone"),         QStringLiteral("pulsestyle_onevent"),     QStringLiteral("typedef"),
        QStringLiteral("constraint"),     QStringLiteral("ignore_bins"),    QStringLiteral("pulsestyle_ondetect"),    QStringLiteral("union"),
        QStringLiteral("context"),        QStringLiteral("illegal_bins"),   QStringLiteral("pure"),                   QStringLiteral("unique"),
        QStringLiteral("continue"),       QStringLiteral("import"),         QStringLiteral("rand"),                   QStringLiteral("unsigned"),
        QStringLiteral("cover"),          QStringLiteral("incdir"),         QStringLiteral("randc"),                  QStringLiteral("use"),
        QStringLiteral("covergroup"),     QStringLiteral("include"),        QStringLiteral("randcase"),               QStringLiteral("uwire"),
        QStringLiteral("coverpoint"),     QStringLiteral("initial"),        QStringLiteral("randsequence"),           QStringLiteral("var"),
        QStringLiteral("cross"),          QStringLiteral("inout"),          QStringLiteral("rcmos"),                  QStringLiteral("vectored"),
        QStringLiteral("deassign"),       QStringLiteral("input"),          QStringLiteral("real"),                   QStringLiteral("virtual"),
        QStringLiteral("default"),        QStringLiteral("inside"),         QStringLiteral("realtime"),               QStringLiteral("void"),
        QStringLiteral("defparam"),       QStringLiteral("instance"),       QStringLiteral("ref"),                    QStringLiteral("wait"),
        QStringLiteral("design"),         QStringLiteral("int"),            QStringLiteral("reg"),                    QStringLiteral("wait_order"),
        QStringLiteral("disable"),        QStringLiteral("integer"),        QStringLiteral("release"),                QStringLiteral("wand"),
        QStringLiteral("dist"),           QStringLiteral("interface"),      QStringLiteral("repeat"),                 QStringLiteral("weak0"),
        QStringLiteral("do"),             QStringLiteral("intersect"),      QStringLiteral("return"),                 QStringLiteral("weak1"),
        QStringLiteral("edge"),           QStringLiteral("join"),           QStringLiteral("rnmos"),                  QStringLiteral("while"),
        QStringLiteral("else"),           QStringLiteral("join_any"),       QStringLiteral("rpmos"),                  QStringLiteral("wildcard"),
        QStringLiteral("end"),            QStringLiteral("join_none"),      QStringLiteral("rtran"),                  QStringLiteral("wire"),
        QStringLiteral("endcase"),        QStringLiteral("large"),          QStringLiteral("tranif0"),                QStringLiteral("with"),
        QStringLiteral("endclass"),       QStringLiteral("liblist"),        QStringLiteral("tranif1"),                QStringLiteral("within"),
        QStringLiteral("endclocking"),    QStringLiteral("library"),        QStringLiteral("scalared"),               QStringLiteral("wor"),
        QStringLiteral("endconfig"),      QStringLiteral("local"),          QStringLiteral("sequence"),               QStringLiteral("xnor"),
        QStringLiteral("endfunction"),    QStringLiteral("localparam"),     QStringLiteral("shortint"),               QStringLiteral("xor"),
        QStringLiteral("endgenerate"),    QStringLiteral("logic"),          QStringLiteral("shortreal"),
        QStringLiteral("endgroup"),       QStringLiteral("longint"),        QStringLiteral("showcancelled"),
        QStringLiteral("endinterface"),   QStringLiteral("macromodule"),    QStringLiteral("signed")
    };
    keywords_1800_2005.sort();
    setModel(new QStringListModel(keywords_1800_2005, this));
    setCaseSensitivity(Qt::CaseInsensitive);
}
