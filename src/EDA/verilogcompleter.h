/********************************************************************************
  * @file    verilogcompleter.h
  * @author  Lun Li
  * @version V2.2.1
  * @date    2022.9.5
  * @brief   This file contains all the functions prototypes for VerilogCompleter.
  ******************************************************************************/

#ifndef VERILOGCOMPLETER_H
#define VERILOGCOMPLETER_H

#include <QCompleter>

/************
  * VerilogCompleter类，用于在编辑器中为verilog语法提供代码补全功能
  ***********/
class VerilogCompleter : public QCompleter
{
    Q_OBJECT
public:
    explicit VerilogCompleter(QObject *parent = nullptr);
};

#endif // VERILOGCOMPLETER_H
