/********************************************************************************
  * @file    verilogsyntaxhighlighter.cpp
  * @author  Lun Li
  * @version V2.2.3
  * @date    2022.9.14
  * @brief   This file provides all the VerilogSyntaxHighlighter functions.
  *******************************************************************************/

#include "verilogsyntaxhighlighter.h"

VerilogSyntaxHighlighter::VerilogSyntaxHighlighter(ProgramConfig *const config,
                                                   QTextDocument *parent)
    : QSyntaxHighlighter(parent),
      m_config(config)
{
    HighlightingRule rule;

    keywordFormat.setForeground(config->keywordColor());
    keywordFormat.setFontWeight(QFont::Bold);

#if 0
    // IEEE 1364-2005 下的关键字列表
    const QString keywordPatterns_1364_2005[] = {
        QStringLiteral("\\balways\\b"),         QStringLiteral("\\bifnone\\b"),             QStringLiteral("\\brnmos\\b"),
        QStringLiteral("\\band\\b"),            QStringLiteral("\\bincdir\\b"),             QStringLiteral("\\brpmos\\b"),
        QStringLiteral("\\bassign\\b"),         QStringLiteral("\\binclude\\b"),            QStringLiteral("\\brtran\\b"),
        QStringLiteral("\\bautomatic\\b"),      QStringLiteral("\\binitial\\b"),            QStringLiteral("\\brtranif0\\b"),
        QStringLiteral("\\bbegin\\b"),          QStringLiteral("\\binout\\b"),              QStringLiteral("\\brtranif1\\b"),
        QStringLiteral("\\bbuf\\b"),            QStringLiteral("\\binput\\b"),              QStringLiteral("\\bscalared\\b"),
        QStringLiteral("\\bbufif0\\b"),         QStringLiteral("\\binstance\\b"),           QStringLiteral("\\bshowcancelled\\b"),
        QStringLiteral("\\bbufif1\\b"),         QStringLiteral("\\binteger\\b"),            QStringLiteral("\\bsigned\\b"),
        QStringLiteral("\\bcase\\b"),           QStringLiteral("\\bjoin\\b"),               QStringLiteral("\\bsmall\\b"),
        QStringLiteral("\\bcasex\\b"),          QStringLiteral("\\blarge\\b"),              QStringLiteral("\\bspecify\\b"),
        QStringLiteral("\\bcasez\\b"),          QStringLiteral("\\bliblist\\b"),            QStringLiteral("\\bspecparam\\b"),
        QStringLiteral("\\bcell\\b"),           QStringLiteral("\\blibrary\\b"),            QStringLiteral("\\bstrong0\\b"),
        QStringLiteral("\\bcmos\\b"),           QStringLiteral("\\blocalparam\\b"),         QStringLiteral("\\bstrong1\\b"),
        QStringLiteral("\\bconfig\\b"),         QStringLiteral("\\bmacromodule\\b"),        QStringLiteral("\\bsupply0\\b"),
        QStringLiteral("\\bdeassign\\b"),       QStringLiteral("\\bmedium\\b"),             QStringLiteral("\\bsupply1\\b"),
        QStringLiteral("\\bdefault\\b"),        QStringLiteral("\\bmodule\\b"),             QStringLiteral("\\btable\\b"),
        QStringLiteral("\\bdefparam\\b"),       QStringLiteral("\\bnand\\b"),               QStringLiteral("\\btask\\b"),
        QStringLiteral("\\bdesign\\b"),         QStringLiteral("\\bnegedge\\b"),            QStringLiteral("\\btime\\b"),
        QStringLiteral("\\bdisable\\b"),        QStringLiteral("\\bnmos\\b"),               QStringLiteral("\\btran\\b"),
        QStringLiteral("\\bedge\\b"),           QStringLiteral("\\bnor\\b"),                QStringLiteral("\\btranif0\\b"),
        QStringLiteral("\\belse\\b"),           QStringLiteral("\\bnoshowcancelled\\b"),    QStringLiteral("\\btranif1\\b"),
        QStringLiteral("\\bend\\b"),            QStringLiteral("\\bnot\\b"),                QStringLiteral("\\btri\\b"),
        QStringLiteral("\\bendcase\\b"),        QStringLiteral("\\bnotif0\\b"),             QStringLiteral("\\btri0\\b"),
        QStringLiteral("\\bendconfig\\b"),      QStringLiteral("\\bnotif1\\b"),             QStringLiteral("\\btri1\\b"),
        QStringLiteral("\\bendfunction\\b"),    QStringLiteral("\\bor\\b"),                 QStringLiteral("\\btriand\\b"),
        QStringLiteral("\\bendgenerate\\b"),    QStringLiteral("\\boutput\\b"),             QStringLiteral("\\btrior\\b"),
        QStringLiteral("\\bendmodule\\b"),      QStringLiteral("\\bparameter\\b"),          QStringLiteral("\\btrireg\\b"),
        QStringLiteral("\\bendprimitive\\b"),   QStringLiteral("\\bpmos\\b"),               QStringLiteral("\\bunsigned\\b"),
        QStringLiteral("\\bendspecify\\b"),     QStringLiteral("\\bposedge\\b"),            QStringLiteral("\\buse\\b"),
        QStringLiteral("\\bendtable\\b"),       QStringLiteral("\\bprimitive\\b"),          QStringLiteral("\\buwire\\b"),
        QStringLiteral("\\bendtask\\b"),        QStringLiteral("\\bpull0\\b"),              QStringLiteral("\\bvectored\\b"),
        QStringLiteral("\\bevent\\b"),          QStringLiteral("\\bpull1\\b"),              QStringLiteral("\\bwait\\b"),
        QStringLiteral("\\bfor\\b"),            QStringLiteral("\\bpulldown\\b"),           QStringLiteral("\\bwand\\b"),
        QStringLiteral("\\bforce\\b"),          QStringLiteral("\\bpullup\\b"),             QStringLiteral("\\bweak0\\b"),
        QStringLiteral("\\bforever\\b"),        QStringLiteral("\\bpulsestyle_onevent\\b"), QStringLiteral("\\bweak1\\b"),
        QStringLiteral("\\bfork\\b"),           QStringLiteral("\\bpulsestyle_ondetect\\b"),QStringLiteral("\\bwhile\\b"),
        QStringLiteral("\\bfunction\\b"),       QStringLiteral("\\brcmos\\b"),              QStringLiteral("\\bwire\\b"),
        QStringLiteral("\\bgenerate\\b"),       QStringLiteral("\\breal\\b"),               QStringLiteral("\\bwor\\b"),
        QStringLiteral("\\bgenvar\\b"),         QStringLiteral("\\brealtime\\b"),           QStringLiteral("\\bxnor\\b"),
        QStringLiteral("\\bhighz0\\b"),         QStringLiteral("\\breg\\b"),                QStringLiteral("\\bxor\\b"),
        QStringLiteral("\\bhighz1\\b"),         QStringLiteral("\\brelease\\b"),
        QStringLiteral("\\bif\\b"),             QStringLiteral("\\brepeat\\b")
    };
#endif

    // IEEE 1800-2005(SystemVerilog) 下的关键字列表
    const QString keywordPatterns_1800_2005[] = {
        QStringLiteral("\\balias\\b"),          QStringLiteral("\\bendmodule\\b"),      QStringLiteral("\\bmatches\\b"),                QStringLiteral("\\bsmall\\b"),
        QStringLiteral("\\balways\\b"),         QStringLiteral("\\bendpackage\\b"),     QStringLiteral("\\bmedium\\b"),                 QStringLiteral("\\bsolve\\b"),
        QStringLiteral("\\balways_comb\\b"),    QStringLiteral("\\bendprimitive\\b"),   QStringLiteral("\\bmodport\\b"),                QStringLiteral("\\bspecify\\b"),
        QStringLiteral("\\balways_ff\\b"),      QStringLiteral("\\bendprogram\\b"),     QStringLiteral("\\bmodule\\b"),                 QStringLiteral("\\bspecparam\\b"),
        QStringLiteral("\\balways_latch\\b"),   QStringLiteral("\\bendproperty\\b"),    QStringLiteral("\\bnand\\b"),                   QStringLiteral("\\bstatic\\b"),
        QStringLiteral("\\band\\b"),            QStringLiteral("\\bendspecify\\b"),     QStringLiteral("\\bnegedge\\b"),                QStringLiteral("\\bstring\\b"),
        QStringLiteral("\\bassert\\b"),         QStringLiteral("\\bendsequence\\b"),    QStringLiteral("\\bnew\\b"),                    QStringLiteral("\\bstrong0\\b"),
        QStringLiteral("\\bassign\\b"),         QStringLiteral("\\bendtable\\b"),       QStringLiteral("\\bnmos\\b"),                   QStringLiteral("\\bstrong1\\b"),
        QStringLiteral("\\bassume\\b"),         QStringLiteral("\\bendtask\\b"),        QStringLiteral("\\bnor\\b"),                    QStringLiteral("\\bstruct\\b"),
        QStringLiteral("\\bautomatic\\b"),      QStringLiteral("\\benum\\b"),           QStringLiteral("\\bnoshowcancelled\\b"),        QStringLiteral("\\bsuper\\b"),
        QStringLiteral("\\bbefore\\b"),         QStringLiteral("\\bevent\\b"),          QStringLiteral("\\bnot\\b"),                    QStringLiteral("\\bsupply0\\b"),
        QStringLiteral("\\bbegin\\b"),          QStringLiteral("\\bexpect\\b"),         QStringLiteral("\\bnotif0\\b"),                 QStringLiteral("\\bsupply1\\b"),
        QStringLiteral("\\bbind\\b"),           QStringLiteral("\\bexport\\b"),         QStringLiteral("\\bnotif1\\b"),                 QStringLiteral("\\btable\\b"),
        QStringLiteral("\\bbins\\b"),           QStringLiteral("\\bextends\\b"),        QStringLiteral("\\bnull\\b"),                   QStringLiteral("\\btagged\\b"),
        QStringLiteral("\\bbinsof\\b"),         QStringLiteral("\\bextern\\b"),         QStringLiteral("\\bor\\b"),                     QStringLiteral("\\btask\\b"),
        QStringLiteral("\\bbit\\b"),            QStringLiteral("\\bfinal\\b"),          QStringLiteral("\\boutput\\b"),                 QStringLiteral("\\bthis\\b"),
        QStringLiteral("\\bbreak\\b"),          QStringLiteral("\\bfirst_match\\b"),    QStringLiteral("\\bpackage\\b"),                QStringLiteral("\\bthroughout\\b"),
        QStringLiteral("\\bbuf\\b"),            QStringLiteral("\\bfor\\b"),            QStringLiteral("\\bpacked\\b"),                 QStringLiteral("\\btime\\b"),
        QStringLiteral("\\bbufif0\\b"),         QStringLiteral("\\bforce\\b"),          QStringLiteral("\\bparameter\\b"),              QStringLiteral("\\btimeprecision\\b"),
        QStringLiteral("\\bbufif1\\b"),         QStringLiteral("\\bforeach\\b"),        QStringLiteral("\\bpmos\\b"),                   QStringLiteral("\\btimeunit\\b"),
        QStringLiteral("\\bbyte\\b"),           QStringLiteral("\\bforever\\b"),        QStringLiteral("\\bposedge\\b"),                QStringLiteral("\\btran\\b"),
        QStringLiteral("\\bcase\\b"),           QStringLiteral("\\bfork\\b"),           QStringLiteral("\\bprimitive\\b"),              QStringLiteral("\\btranif0\\b"),
        QStringLiteral("\\bcasex\\b"),          QStringLiteral("\\bforkjoin\\b"),       QStringLiteral("\\bpriority\\b"),               QStringLiteral("\\btranif1\\b"),
        QStringLiteral("\\bcasez\\b"),          QStringLiteral("\\bfunction\\b"),       QStringLiteral("\\bprogram\\b"),                QStringLiteral("\\btri\\b"),
        QStringLiteral("\\bcell\\b"),           QStringLiteral("\\bgenerate\\b"),       QStringLiteral("\\bproperty\\b"),               QStringLiteral("\\btri0\\b"),
        QStringLiteral("\\bchandle\\b"),        QStringLiteral("\\bgenvar\\b"),         QStringLiteral("\\bprotected\\b"),              QStringLiteral("\\btri1\\b"),
        QStringLiteral("\\bclass\\b"),          QStringLiteral("\\bhighz0\\b"),         QStringLiteral("\\bpull0\\b"),                  QStringLiteral("\\btriand\\b"),
        QStringLiteral("\\bclocking\\b"),       QStringLiteral("\\bhighz1\\b"),         QStringLiteral("\\bpull1\\b"),                  QStringLiteral("\\btrior\\b"),
        QStringLiteral("\\bcmos\\b"),           QStringLiteral("\\bif\\b"),             QStringLiteral("\\bpulldown\\b"),               QStringLiteral("\\btrireg\\b"),
        QStringLiteral("\\bconfig\\b"),         QStringLiteral("\\biff\\b"),            QStringLiteral("\\bpullup\\b"),                 QStringLiteral("\\btype\\b"),
        QStringLiteral("\\bconst\\b"),          QStringLiteral("\\bifnone\\b"),         QStringLiteral("\\bpulsestyle_onevent\\b"),     QStringLiteral("\\btypedef\\b"),
        QStringLiteral("\\bconstraint\\b"),     QStringLiteral("\\bignore_bins\\b"),    QStringLiteral("\\bpulsestyle_ondetect\\b"),    QStringLiteral("\\bunion\\b"),
        QStringLiteral("\\bcontext\\b"),        QStringLiteral("\\billegal_bins\\b"),   QStringLiteral("\\bpure\\b"),                   QStringLiteral("\\bunique\\b"),
        QStringLiteral("\\bcontinue\\b"),       QStringLiteral("\\bimport\\b"),         QStringLiteral("\\brand\\b"),                   QStringLiteral("\\bunsigned\\b"),
        QStringLiteral("\\bcover\\b"),          QStringLiteral("\\bincdir\\b"),         QStringLiteral("\\brandc\\b"),                  QStringLiteral("\\buse\\b"),
        QStringLiteral("\\bcovergroup\\b"),     QStringLiteral("\\binclude\\b"),        QStringLiteral("\\brandcase\\b"),               QStringLiteral("\\buwire\\b"),
        QStringLiteral("\\bcoverpoint\\b"),     QStringLiteral("\\binitial\\b"),        QStringLiteral("\\brandsequence\\b"),           QStringLiteral("\\bvar\\b"),
        QStringLiteral("\\bcross\\b"),          QStringLiteral("\\binout\\b"),          QStringLiteral("\\brcmos\\b"),                  QStringLiteral("\\bvectored\\b"),
        QStringLiteral("\\bdeassign\\b"),       QStringLiteral("\\binput\\b"),          QStringLiteral("\\breal\\b"),                   QStringLiteral("\\bvirtual\\b"),
        QStringLiteral("\\bdefault\\b"),        QStringLiteral("\\binside\\b"),         QStringLiteral("\\brealtime\\b"),               QStringLiteral("\\bvoid\\b"),
        QStringLiteral("\\bdefparam\\b"),       QStringLiteral("\\binstance\\b"),       QStringLiteral("\\bref\\b"),                    QStringLiteral("\\bwait\\b"),
        QStringLiteral("\\bdesign\\b"),         QStringLiteral("\\bint\\b"),            QStringLiteral("\\breg\\b"),                    QStringLiteral("\\bwait_order\\b"),
        QStringLiteral("\\bdisable\\b"),        QStringLiteral("\\binteger\\b"),        QStringLiteral("\\brelease\\b"),                QStringLiteral("\\bwand\\b"),
        QStringLiteral("\\bdist\\b"),           QStringLiteral("\\binterface\\b"),      QStringLiteral("\\brepeat\\b"),                 QStringLiteral("\\bweak0\\b"),
        QStringLiteral("\\bdo\\b"),             QStringLiteral("\\bintersect\\b"),      QStringLiteral("\\breturn\\b"),                 QStringLiteral("\\bweak1\\b"),
        QStringLiteral("\\bedge\\b"),           QStringLiteral("\\bjoin\\b"),           QStringLiteral("\\brnmos\\b"),                  QStringLiteral("\\bwhile\\b"),
        QStringLiteral("\\belse\\b"),           QStringLiteral("\\bjoin_any\\b"),       QStringLiteral("\\brpmos\\b"),                  QStringLiteral("\\bwildcard\\b"),
        QStringLiteral("\\bend\\b"),            QStringLiteral("\\bjoin_none\\b"),      QStringLiteral("\\brtran\\b"),                  QStringLiteral("\\bwire\\b"),
        QStringLiteral("\\bendcase\\b"),        QStringLiteral("\\blarge\\b"),          QStringLiteral("\\btranif0\\b"),                QStringLiteral("\\bwith\\b"),
        QStringLiteral("\\bendclass\\b"),       QStringLiteral("\\bliblist\\b"),        QStringLiteral("\\btranif1\\b"),                QStringLiteral("\\bwithin\\b"),
        QStringLiteral("\\bendclocking\\b"),    QStringLiteral("\\blibrary\\b"),        QStringLiteral("\\bscalared\\b"),               QStringLiteral("\\bwor\\b"),
        QStringLiteral("\\bendconfig\\b"),      QStringLiteral("\\blocal\\b"),          QStringLiteral("\\bsequence\\b"),               QStringLiteral("\\bxnor\\b"),
        QStringLiteral("\\bendfunction\\b"),    QStringLiteral("\\blocalparam\\b"),     QStringLiteral("\\bshortint\\b"),               QStringLiteral("\\bxor\\b"),
        QStringLiteral("\\bendgenerate\\b"),    QStringLiteral("\\blogic\\b"),          QStringLiteral("\\bshortreal\\b"),
        QStringLiteral("\\bendgroup\\b"),       QStringLiteral("\\blongint\\b"),        QStringLiteral("\\bshowcancelled\\b"),
        QStringLiteral("\\bendinterface\\b"),   QStringLiteral("\\bmacromodule\\b"),    QStringLiteral("\\bsigned\\b")
    };

    for (const QString &pattern : keywordPatterns_1800_2005) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = &keywordFormat;
        highlightingRules.append(rule);
    }

    quotationFormat.setForeground(config->quotationColor());

    numberFormat.setForeground(config->numberColor());
    const QString numberPatterns[] = {
        QStringLiteral("(?<![_a-zA-Z\\$])\\d*\\'[bo]?[0-9xzXZ_]*"),
        QStringLiteral("(?<![_a-zA-Z\\$])\\d*\\'d?[0-9_]*"),
        QStringLiteral("(?<![_a-zA-Z\\$])\\d*\\'h[0-9xzXZa-fA-F_]*"),
        QStringLiteral("(?<![_a-zA-Z\\$])\\b[0-9_]*\\b"),
        QStringLiteral("(?<![_a-zA-Z\\$])-?\\d*\\.\\d(?:[eE]-?\\d*)?\\b")
    };
    for (const QString &pattern : numberPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = &numberFormat;
        highlightingRules.append(rule);
    }

    systemFunctionFormat.setForeground(config->systemFunctionColor());
    rule.pattern = QRegularExpression(QStringLiteral("(?<![_a-zA-Z\\$])\\$\\w+\\b"));
    rule.format = &systemFunctionFormat;
    highlightingRules.append(rule);

    singleLineCommentExpression = QRegularExpression(QStringLiteral("//"));
    commentStartExpression = QRegularExpression(QStringLiteral("/\\*"));
    commentEndExpression = QRegularExpression(QStringLiteral("\\*/"));

    commentFormat.setForeground(config->commentColor());

    beginScopeExpression = QList<QRegularExpression>{QRegularExpression(QStringLiteral("\\bbegin\\b")),
                                                     QRegularExpression(QStringLiteral("\\bcase\\b")),
                                                     QRegularExpression(QStringLiteral("\\bclass\\b")),
                                                     QRegularExpression(QStringLiteral("\\bclocking\\b")),
                                                     QRegularExpression(QStringLiteral("\\bconfig\\b")),
                                                     QRegularExpression(QStringLiteral("\\bfunction\\b")),
                                                     QRegularExpression(QStringLiteral("\\bgenerate\\b")),
                                                     QRegularExpression(QStringLiteral("\\bgroup\\b")),
                                                     QRegularExpression(QStringLiteral("\\binterface\\b")),
                                                     QRegularExpression(QStringLiteral("\\bmodule\\b")),
                                                     QRegularExpression(QStringLiteral("\\bpackage\\b")),
                                                     QRegularExpression(QStringLiteral("\\bprimitive\\b")),
                                                     QRegularExpression(QStringLiteral("\\bprogram\\b")),
                                                     QRegularExpression(QStringLiteral("\\bproperty\\b")),
                                                     QRegularExpression(QStringLiteral("\\bspecify\\b")),
                                                     QRegularExpression(QStringLiteral("\\bsequence\\b")),
                                                     QRegularExpression(QStringLiteral("\\btable\\b")),
                                                     QRegularExpression(QStringLiteral("\\btask\\b"))};

    endScopeExpression = QList<QRegularExpression>{QRegularExpression(QStringLiteral("\\bend\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendcase\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendclass\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendclocking\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendconfig\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendfunction\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendgenerate\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendgroup\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendinterface\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendmodule\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendpackage\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendprimitive\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendprogram\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendproperty\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendspecify\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendsequence\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendtable\\b")),
                                                     QRegularExpression(QStringLiteral("\\bendtask\\b"))};

    connect(config, &ProgramConfig::editorConfigChanged, this, &VerilogSyntaxHighlighter::setColor);
}

void VerilogSyntaxHighlighter::highlightBlock(const QString &text)
{
    // state为0表示当前行不在多行注释内，为1表示在多行注释内
    if (previousBlockState() == 1) {
        setCurrentBlockState(1);
    } else {
        setCurrentBlockState(0);
    }

    int textLength = text.length();
    int startIndex = 0, endIndex = 0;
    bool inQuotation = false;
    bool inComment = previousBlockState() == 1;
    CodeTextBlockUserData *blockData;
    if (currentBlockUserData()) {
        blockData = static_cast<CodeTextBlockUserData *>(currentBlockUserData());
    } else {
        blockData = new CodeTextBlockUserData;
    }
    if (currentBlock().previous().userData()) {
        blockData->indentStack = static_cast<CodeTextBlockUserData *>(currentBlock().previous().userData())->indentStack;
    } else {
        blockData->indentStack = 0;
    }
    blockData->startScopeKeywordNum = 0;
    blockData->endScopeKeywordNum = 0;
    while (endIndex < textLength) {
        bool inChanging = false;
        if (text.at(endIndex) == '\"' && !inQuotation && !inComment) { // 引用开始
            inQuotation = true;
            inChanging = true;
        } else if (text.at(endIndex) == '\"' && inQuotation && !inComment) { // 引用结束
            setFormat(startIndex, endIndex - startIndex + 1, quotationFormat);
            inQuotation = false;
            startIndex = endIndex + 1;
        } else if (endIndex < textLength - 1) {
            if (text.at(endIndex) == '/' && text.at(endIndex + 1) == '*' && !inQuotation && !inComment) { // 多行注释开始
                inComment = true;
                inChanging = true;
                setCurrentBlockState(1);
            } else if (text.at(endIndex) == '*' && text.at(endIndex + 1) == '/' && !inQuotation && inComment) { // 多行注释结束
                ++endIndex;
                setFormat(startIndex, endIndex - startIndex + 1, quotationFormat);
                inComment = false;
                setCurrentBlockState(0);
                startIndex = endIndex + 1;
            } else if (text.at(endIndex) == '/' && text.at(endIndex + 1) == '/' && !inQuotation && !inComment) { // 单行注释
                highlightKeywords(text, startIndex, endIndex, blockData);
                setFormat(startIndex, textLength - startIndex, commentFormat);
                setCurrentBlockUserData(blockData);
                return;
            }
        }
        if (inChanging) {
            highlightKeywords(text, startIndex, endIndex, blockData);
            startIndex = endIndex;
        }
        ++endIndex;
    }
    if (startIndex < textLength) {
        if (inComment) {
            setFormat(startIndex, textLength - startIndex, commentFormat);
        } else {
            highlightKeywords(text, startIndex, endIndex, blockData);
        }
    }    
    setCurrentBlockUserData(blockData);
}

void VerilogSyntaxHighlighter::setColor()
{
    keywordFormat.setForeground(m_config->keywordColor());
    quotationFormat.setForeground(m_config->quotationColor());
    numberFormat.setForeground(m_config->numberColor());
    commentFormat.setForeground(m_config->commentColor());
    systemFunctionFormat.setForeground(m_config->systemFunctionColor());
    rehighlight();
}

void VerilogSyntaxHighlighter::highlightKeywords(const QString &text, int startIndex, int endIndex, CodeTextBlockUserData *blockData)
{
    QString subText = text.sliced(startIndex, endIndex - startIndex);

    // 高亮所有关键字
    for (const HighlightingRule &rule : qAsConst(highlightingRules)) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(subText);
        while (matchIterator.hasNext()) { // 匹配到关键字
            QRegularExpressionMatch match = matchIterator.next();
            if (beginScopeExpression.contains(rule.pattern)) { // 检测到范围起始关键字
                blockData->indentStack++; // 缩进+1
                blockData->startScopeKeywordNum++;
            }
            if (endScopeExpression.contains(rule.pattern)) {
                blockData->indentStack--;
                blockData->endScopeKeywordNum++;
            }
            setFormat(match.capturedStart() + startIndex, match.capturedLength(), *rule.format);
        }
    }
}
