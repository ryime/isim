/********************************************************************************
  * @file    verilogsyntaxhighlighter.h
  * @author  Lun Li
  * @version V2.2.3
  * @date    2022.9.14
  * @brief   This file contains all the functions prototypes for VerilogSyntaxHighlighter.
  ******************************************************************************/

#ifndef VERILOGSYNTAXHIGHLIGHTER_H
#define VERILOGSYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QRegularExpression>
#include "programconfig.h"
#include "codetextblockuserdata.h"

/************
  * VerilogSyntaxHighlighter类，用于在编辑器中为verilog语法提供高亮显示
  ***********/
class VerilogSyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    VerilogSyntaxHighlighter(ProgramConfig *const config, QTextDocument *parent = nullptr);

protected:
    void highlightBlock(const QString &text) override;

private slots:
    void setColor();

private:
    struct HighlightingRule
    {
        QRegularExpression pattern;
        QTextCharFormat *format;
    };
    QList<HighlightingRule> highlightingRules;

    QRegularExpression singleLineCommentExpression;
    QRegularExpression commentStartExpression;
    QRegularExpression commentEndExpression;

    QTextCharFormat keywordFormat;
    QTextCharFormat numberFormat;
    QTextCharFormat commentFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat systemFunctionFormat;

    QList<QRegularExpression> beginScopeExpression;
    QList<QRegularExpression> endScopeExpression;
    ProgramConfig *const m_config;

    // 高亮关键字，在highlightBlock函数里调用，高亮的范围为[startIndex, endIndex)
    void highlightKeywords(const QString &text,
                           int startIndex,
                           int endIndex,
                           CodeTextBlockUserData *blockData);
};

#endif // VERILOGSYNTAXHIGHLIGHTER_H
