/********************************************************************************
  * @file    waveprocess.h
  * @author  Lun Li
  * @version V2.2.0
  * @date    2022.7.8
  * @brief   This file contains all the functions prototypes for the WaveProcess.
  ******************************************************************************/

#ifndef WAVEPROCESS_H
#define WAVEPROCESS_H

#include <QProcess>
#include "programconfig.h"
#include "signalconnection.h"

/************
  * WaveProcess类，用于给用户提供命令行，完成波形显示
  ***********/
class WaveProcess : public QProcess
{
    Q_OBJECT
public:
    explicit WaveProcess(const ProgramConfig *config, QObject *parent = nullptr);
    ~WaveProcess();
    void setWorkingDirectory(const QString &dir);

    void runCommand(const QString &command);
    void addCommand(const QString &command);

    void openLog();
    void closeLog();

signals:
    void waveStarted();
    void waveInput(const QString &input);
    void waveOutput(const QString &output);
    void waveError(const QString &error);
    void waveFinished();

private:
    void iverilogCmdTransform(QStringList &commands);
    void vvpCmdTransform(QStringList &commands);

    const ProgramConfig *m_config;

    bool m_isStarted = false;

    QFile inputLog, outputLog;
    QTextStream inputStream, outputStream;
    bool m_isLogEnabled;
};

#endif // WAVEPROCESS_H
